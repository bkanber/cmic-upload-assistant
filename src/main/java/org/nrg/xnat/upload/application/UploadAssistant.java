/*
 * upload-assistant: org.nrg.xnat.upload.application.UploadAssistant
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.application;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.netbeans.api.wizard.WizardDisplayer;
import org.netbeans.api.wizard.WizardResultReceiver;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.nrg.dicom.mizer.service.MizerService;
import org.nrg.framework.concurrency.LoggingThreadPoolExecutor;
import org.nrg.framework.pinto.PintoApplication;
import org.nrg.framework.pinto.PintoException;
import org.nrg.xnat.upload.data.AssignedSessionVariable;
import org.nrg.xnat.upload.net.RestServer;
import org.nrg.xnat.upload.net.XnatServer;
import org.nrg.xnat.upload.ui.*;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.prefs.BackingStoreException;

import static org.nrg.xnat.upload.data.SessionVariableNames.PROTOCOL_LABEL;
import static org.nrg.xnat.upload.data.SessionVariableNames.VISIT_LABEL;

@PintoApplication(value = "XNAT Upload Assistant", version = "property:version", copyright = "Copyright (c) 2017, Washington University School of Medicine")
public class UploadAssistant extends JFrame implements WizardResultReceiver {
    public static void main(final String[] args) {
        if (_log.isDebugEnabled()) {
            _log.debug("I have entered the main() method for the UploadAssistant class.");
        }
        try {
            final UploadAssistant application = new UploadAssistant(args);
            application.start();
            application.setVisible(true);
        } catch (IOException e) {
            _log.error("An error occurred running the upload assistant application", e);
        } catch (PintoException e) {
            _log.error("An error occurred processing command-line arguments for the upload assistant", e);
        } catch (BackingStoreException e) {
            _log.error("An error occurred accessing the Java Preferences store for the upload assistant", e);
        }
    }

    /**
     * Default constructor.
     *
     * @param args Command-line arguments for the application.
     *
     * @throws IOException           When something goes wrong reading data.
     * @throws PintoException        When an error occurs processing command-line arguments.
     * @throws BackingStoreException When an error occurs accessing the Java Preferences store.
     */
    public UploadAssistant(final String[] args) throws IOException, PintoException, BackingStoreException {
        super();

        _instance = this;

        if (_log.isDebugEnabled()) {
            _log.debug("I have entered the constructor for the UploadAssistant class.");
        }

        _settings = new UploadAssistantSettings(this, args);
        if (!_settings.getShouldContinue()) {
            _log.error("There was a start-up error related to command-line parameters or arguments: {}", _settings.getHelp());
        }

        _restServer = new RestServer(_settings);

        final String title = _settings.getProperty(Constants.APPLICATION_NAME, Messages.getMessage("application.name"));
        _settings.setProperty(Constants.APPLICATION_NAME, title);
        setTitle(title);

        _context = new AnnotationConfigApplicationContext(ApplicationConfig.class);

        setDefaultLookAndFeelDecorated(true);
        try {
            for (final UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (Exception ignore) {
                // We'll ignore this: it might look not so nice but it should still work.
            }
        }

        setIconImage(new ImageIcon(getClass().getResource("/org/nrg/xnat/upload/xnat.png")).getImage());
        setSize(900, 600);
        setResizable(false);

        HttpURLConnection.setFollowRedirects(false);

        configureLookAndFeel();
        configureLogging();

        if (_log.isDebugEnabled()) {
            _log.debug("The upload assistant has been constructed and can now start up.");
        }
    }

    public static RestServer getRestServer() {
        return _instance._restServer;
    }

    public static UploadAssistantSettings getSettings() {
        return _instance._settings;
    }

    public static XnatServer getCurrentXnatServer() {
        return _instance._settings.getCurrentXnatServer();
    }

    public static MizerService getMizerService() {
        return _instance._context.getBean(MizerService.class);
    }

    public static UploadAssistant getInstance() {
        return _instance;
    }

    /**
     */
    private void start() {
        _log.trace("START {}", this);

        final Dimension dimension = new Dimension(300, 300);

        try {
            _log.trace("{} initializing pages", this);
            LoggingThreadPoolExecutor.setUseLoggingExecutor(_settings.isUseLoggingExecutor());

            final JFrame component = this;
            final String title = _settings.getProperty(Constants.APPLICATION_NAME);
            final ClearPreferencesAction clearPreferencesAction = new ClearPreferencesAction(component, _settings);

            // We need an alias for this to use inside the object initialization blocks below.
            setJMenuBar(new JMenuBar() {{
                add(new JMenu(Messages.getMessage("menu.info.title")) {{
                    setMnemonic(KeyEvent.VK_I);
                    add(new JMenuItem(Messages.getMessage("menu.about.title", title)) {{
                        setMnemonic(KeyEvent.VK_A);
                        addActionListener(new AboutAction(component, _settings));
                    }});
                    if (Messages.getSupportedLocales().size() > 1) {
                        add(new JMenuItem(Messages.getMessage("menu.locale.title")) {{
                            setMnemonic(KeyEvent.VK_L);
                            addActionListener(new ChooseLocaleAction(component, _settings));
                        }});
                    }
                    add(new JMenuItem(Messages.getMessage("menu.clear.title")) {{
                        setMnemonic(KeyEvent.VK_A);
                        addActionListener(clearPreferencesAction);
                    }});
                    add(new JMenuItem(new ExitAction()));
                }});
            }});

            final CloseHandler closeHandler = new CloseHandler(this, title);
            addWindowListener(closeHandler);
            setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

            final List<WizardPage> pages = Lists.newArrayList();
            final Map<String, Object> params = Maps.newLinkedHashMap();

            // First thing is to log into a server.
            final XnatServerAuthenticationPage authenticationPage = new XnatServerAuthenticationPage(_settings);
            clearPreferencesAction.addPropertyChangeListener(authenticationPage);
            pages.add(authenticationPage);

            // TODO: Try to get rid of all these stored in the params map and just use the settings object where appropriate.
            final String projectName = _settings.getProject();
            final boolean hasProjectName = StringUtils.isNotBlank(projectName);
            if (hasProjectName) {
                throw new UnsupportedOperationException("XNAT Upload Assistant does not yet support pre-defined project uploads.");
                // _log.trace("Using project: {}", projectName);
                // _settings.setCurrentProject(new Project(projectName));
            } else {
                pages.add(new SelectProjectPage(dimension));
                _log.trace("project");
            }

            final String subjectName = _settings.getSubject();
            if (StringUtils.isNotBlank(subjectName)) {
                // if (!hasProjectName) {
                    // TODO: use a placeholder?
                //    throw new UnsupportedOperationException("can't assign subject without project");
                // } else {
                //    final Project project = _settings.getCurrentProject();
                //    for (final Subject subject : project.getSubjects()) {
                //        if (subjectName.equals(subject.getLabel()) || subjectName.equals(subject.getId())) {
                //            _settings.setCurrentSubject(subject);
                //            _log.trace("Found subject {}", subjectName);
                //            break;
                //        }
                //    }
                // }
                throw new UnsupportedOperationException("XNAT Upload Assistant does not yet support pre-defined project/subject uploads.");
            }

            if (_settings.getCurrentSubject() == null) {
                pages.add(new SelectSubjectPage(dimension));
                _log.trace("Added select subject page");
            }

            final String visitLabel = _settings.getVisit();
            //TODO: we want to verify this visit... and if there isn't a visit, we want to check the project to see if it has a protocol
            //so we can get a list of visits and let the user associate the session with a visit.
            //For now, we'll just assume the visit is a valid, existing label for a pVisitdata and pass it through to the importer.
            if (StringUtils.isNotBlank(visitLabel)) {
                _log.trace("visit: {}", visitLabel);
                params.put(VISIT_LABEL, new AssignedSessionVariable(VISIT_LABEL, visitLabel));
            }

            final String protocolLabel = _settings.getProtocol();
            //TODO: we want to verify this experiment's protocol is valid for this visit... and if there isn't a visit, we want to check the project to see
            //if it has a protocol so we can get a list of visits and let the user associate the session with a visit.
            //For now, we'll just assume the protocol is valid and pass it through to the importer.
            if (StringUtils.isNotBlank(protocolLabel)) {
                _log.trace("protocol: {}", protocolLabel);
                params.put(PROTOCOL_LABEL, new AssignedSessionVariable(PROTOCOL_LABEL, protocolLabel));
            }

            // Allow the system configuration to override the session date confirmation page, but default to showing it.
            final Date scanDate = _settings.getScanDate();
            if (UIUtils.getConfirmSessionDatePage()) {
                if (scanDate == null) {
                    getSettings().setCurrentSessionDate(Constants.NO_DATE);
                    pages.add(new ConfirmSessionDatePage());
                } else if (scanDate.getTime() == 0) {
                    getSettings().setCurrentSessionDate(scanDate);
                }
            }
            pages.add(new SelectFilesPage());
            _log.trace("files");
            if (StringUtils.isBlank(projectName)) {
                pages.add(new SelectSessionPage());
            } else {
                pages.add(new SelectSessionPage(projectName));
            }
            _log.trace("session");
            pages.add(new AssignSessionVariablesPage());
            _log.trace("session variables");
            _log.trace("{} creating wizard", this);
            final Wizard wizard = WizardPage.createWizard(pages.toArray(new WizardPage[pages.size()]), new UploadWizardResultProducer(this, Executors.newCachedThreadPool()));
            _log.trace("{} installing displayer", this);
            final WizardDisplayer displayer = WizardDisplayer.installInContainer(this, null, wizard, null, params, this);
            displayer.setCloseHandler(closeHandler);
            // final NavButtonManager buttons = ((WizardDisplayerImpl) displayer).getButtonManager();
        } catch (Throwable t) {
            displayError(t);
        }

        _log.trace("{} ready", this);
    }

    /**
     * Implementation of the {@link WizardResultReceiver#cancelled(Map)} method.
     *
     * @see WizardResultReceiver#cancelled(Map)
     */
    @Override
    public void cancelled(@SuppressWarnings("rawtypes") Map args) {
        _log.info("The user requested to cancel.");
        final Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();

        if (window != null) {
            window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
        }
    }

    /**
     * Implementation of the {@link WizardResultReceiver#finished(Object)} method.
     *
     * @see WizardResultReceiver#finished(Object)
     */
    @Override
    public void finished(Object args) {
        _log.info("The wizard is finished.");
        _finished = true;
    }

    private boolean isFinished() {
        return _finished;
    }

    private void displayError(final Throwable t) {
        _log.error("Application startup failed", t);
        final StringWriter sw = new StringWriter();
        final PrintWriter writer = new PrintWriter(sw);
        writer.println("Application startup failed; please contact your XNAT administrator for help.");
        writer.println("Error details: " + t.getMessage());
        t.printStackTrace(writer);
        final JTextArea text = new JTextArea(sw.toString());
        text.setEditable(false);
        add(text);
        validate();
    }

    /**
     * Load the UIManager system look and feel.
     */
    private void configureLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Loads logging resources, including loading logging properties from custom URLs specified by the {@link
     * UploadAssistantSettings#getLog4jUrl()} parameter.
     */
    private void configureLogging() {
        final String log4jProps = _settings.getLog4jUrl();
        if (StringUtils.isNotBlank(log4jProps)) {
            try {
                PropertyConfigurator.configure(new URL(log4jProps));
            } catch (MalformedURLException e) {
                _log.error("Unable to read remote log4j configuration file " + log4jProps, e);
            }
        }
    }

    private static class CloseHandler extends WindowAdapter implements ActionListener {
        public CloseHandler(final UploadAssistant parent, final String applicationName) {
            _parent = parent;
            _applicationName = applicationName;
        }

        @Override
        public void windowClosing(final WindowEvent event) {
            //  Find the active window before creating and dispatching the event
            final Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();

            if (window != null) {
                if (!_parent.isFinished()) {
                    final int confirm = JOptionPane.showOptionDialog(window,
                                                                     Messages.getFormattedMessage("dialog.exit.message", _applicationName),
                                                                     Messages.getMessage("dialog.exit.title"), JOptionPane.YES_NO_OPTION,
                                                                     JOptionPane.QUESTION_MESSAGE, null, null, null);
                    if (confirm == JOptionPane.YES_OPTION) {
                        _parent.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                    }
                }
            }
        }

        @Override
        public void actionPerformed(final ActionEvent event) {
            //  Find the active window before creating and dispatching the event
            final Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
            _parent.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
        }

        private final UploadAssistant _parent;
        private final String          _applicationName;
    }

    private static final Logger _log = LoggerFactory.getLogger(UploadAssistant.class);

    private static UploadAssistant _instance;

    private final RestServer              _restServer;
    private final UploadAssistantSettings _settings;
    private final ApplicationContext      _context;

    private boolean _finished = false;
}
