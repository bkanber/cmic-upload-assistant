/*
 * upload-assistant: org.nrg.xnat.upload.data.IndexedDependentSessionVariable
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.data;

import org.apache.commons.lang3.StringUtils;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;

public class IndexedDependentSessionVariable extends AbstractSessionVariable implements SessionVariable, DocumentListener {
    /**
     * @param name       Name of the session variable.
     * @param dependency The session variable on which this indexed session variable is dependent. This is used for
     *                   updating the value of the control.
     * @param format     A string format for composing the variable value.
     * @param validator  The object that validates and verifies the control value.
     */
    public IndexedDependentSessionVariable(final String name,
                                           final SessionVariable dependency,
                                           final String format,
                                           final ValueValidator validator) {
        super(name);
        this.format = format;
        this.validator = validator;
        setDependency(dependency);
        text = new JTextField(getValidName());
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
     */
    @Override
    public void changedUpdate(final DocumentEvent e) {
        edit();
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.SessionVariable#getEditor()
     */
    @Override
    public Component getEditor() {
        return text;
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.SessionVariable#getValue()
     */
    @Override
    public synchronized Value getValue() {
        final Value  value = super.getValue();
        final String text  = this.text.getText();
        if (value == null || !StringUtils.equals(value.asString(), value.asString())) {
            setValue(new ConstantValue(text));
        }
        return super.getValue();
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
     */
    @Override
    public void insertUpdate(final DocumentEvent e) {
        edit();
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.xnat.upload.data.SessionVariable#isHidden()
     */
    @Override
    public boolean isHidden() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.nrg.xnat.upload.data.SessionVariable#refresh()
     */
    @Override
    public void refresh() {
        try {
            if (!edited) {
                setValue(getValidName());
            } else {
                validate(text.getText());
            }
            fireHasChanged();
        } catch (InvalidValueException e) {
            logger.trace("ignoring change to " + dependency.getName(), e);
        }
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
     */
    @Override
    public void removeUpdate(final DocumentEvent e) {
        edit();
    }

    public SessionVariable getDependency() {
        return dependency;
    }

    /**
     * Overrides the {@link AbstractSessionVariable#setValue(String) base setValue(String)} method to add updating the
     * display value in accordance with the change to the value.
     *
     * @param value The value to set.
     *
     * @return The previously set value of the variable.
     */
    @Override
    public String setValue(final String value) {
        super.setValue(value);
        return updateDisplayValue();
    }

    public void setDependency(SessionVariable dependency) {
        this.dependency = dependency;
    }

    private void edit() {
        edited = true;
        editTo(text.getText());
    }

    private String getValidName() {
        int    i = 1;
        String name;
        do {
            final String value = getDependency() != null && getDependency().getValue() != null
                                 ? getDependency().getValue().asString()
                                 : null;
            name = String.format(format, value, i++);
        } while (!validator.isValid(name));
        return name;
    }

    private final Logger logger = LoggerFactory.getLogger(IndexedDependentSessionVariable.class);
    private final String          format;
    private       SessionVariable dependency;
    private final ValueValidator  validator;
    private final JTextField      text;
    private boolean edited = false;
}
