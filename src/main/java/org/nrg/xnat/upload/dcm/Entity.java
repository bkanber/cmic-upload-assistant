/*
 * upload-assistant: org.nrg.xnat.upload.dcm.Entity
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.dcm;

import java.util.Collection;
import java.util.Map;

public interface Entity {
	Collection<Study> getStudies();
	Collection<Series> getSeriesRegistry();
	Map<Attribute,Object> getAttributes();
	Object get(Attribute a);
}
