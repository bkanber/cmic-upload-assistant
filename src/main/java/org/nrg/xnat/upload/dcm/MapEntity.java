/*
 * upload-assistant: org.nrg.xnat.upload.dcm.MapEntity
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.dcm;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

abstract class MapEntity implements Entity {
	private final Map<Attribute,Object> _map = new HashMap<>();

	/*
	 * (non-Javadoc)
	 * @see Entity#get(Attribute)
	 */
	public Object get(final Attribute attribute) {
		return _map.get(attribute);
	}
	
	public Object get(final int tag) {
		return _map.get(Attribute.Simple.getInstance(tag));
	}
	
	/* (non-Javadoc)
	 * @see Entity#getAttributes()
	 */
	public Map<Attribute,Object> getAttributes() {
		return Collections.unmodifiableMap(_map);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return _map.hashCode();
	}
	
	protected final Object put(final Attribute attribute, final Object value) {
		return _map.put(attribute, value);
	}
	
	protected final Object put(final int tag, final Object value) {
		return _map.put(Attribute.Simple.getInstance(tag), value);
	}
}
