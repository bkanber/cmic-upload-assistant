/*
 * upload-assistant: org.nrg.xnat.upload.io.HttpUtils
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.List;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

public class HttpUtils {
    private HttpUtils() {}

    public static String readEntity(final HttpURLConnection connection) throws IOException {
        InputStream in;
        try {
            in = connection.getInputStream();
            if (null == in) {
                in = connection.getErrorStream();
            }
        } catch (IOException e) {
            in = connection.getErrorStream();
        }
        return null == in ? null : readEntity(in);
    }
    
    public static StringBuilder readEntity(final StringBuilder sb, final InputStream in)
    throws IOException {
        Joiner.on("<p>").appendTo(sb, readEntityLines(in));
        return sb;
    }
    
    public static String readEntity(final InputStream in)
    throws IOException{
        return Joiner.on("<p>").join(readEntityLines(in));
    }


    public static List<String> readEntityLines(final HttpURLConnection c) throws IOException {
        IOException ioexception = null;
        final InputStream in = c.getInputStream();
        try {
            return readEntityLines(in);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                throw null == ioexception ? e : ioexception;
            }
        }
    }
    
    public static List<String> readEntityLines(final InputStream in) throws IOException {
        IOException ioexception = null;
        final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        try {
            final List<String> lines = Lists.newArrayList();
            String line;
            while (null != (line = reader.readLine())) {
                lines.add(line);
            }
            return lines;
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                throw null == ioexception ? e : ioexception;
            }
        }
    }
    
}
