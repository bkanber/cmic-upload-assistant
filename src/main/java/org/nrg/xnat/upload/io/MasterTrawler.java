/*
 * upload-assistant: org.nrg.xnat.upload.io.MasterTrawler
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.nrg.framework.io.EditProgressMonitor;
import org.nrg.framework.io.FileWalkIterator;
import org.nrg.xnat.upload.data.Session;
import org.nrg.xnat.upload.io.dcm.DicomTrawler;
import org.nrg.xnat.upload.io.ecat.EcatTrawler;
import org.nrg.xnat.upload.net.xnat.SeriesImportFilterApplicatorRetriever;
import org.nrg.xnat.upload.util.ArrayIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

public class MasterTrawler implements Callable<List<Session>> {
    private final Logger logger = LoggerFactory.getLogger(MasterTrawler.class);
    // TODO: hard-coded? configurable?
    private final Trawler[]                             trawlers;
    private final Collection<File>                      roots;
    private final EditProgressMonitor                   pm;
    private final SeriesImportFilterApplicatorRetriever filters;

    // TODO: add progress monitor
    public MasterTrawler(final JComponent parent, final EditProgressMonitor monitor, final Iterable<File> files, final SeriesImportFilterApplicatorRetriever filters) {
        this.pm = monitor;
        this.roots = Lists.newArrayList(files);
        this.filters = filters;
        trawlers = new Trawler[]{
                new DicomTrawler(),
                new EcatTrawler(parent),
        };
    }

    @SuppressWarnings("unchecked")
    public List<Session> call() {
        // TODO: a really clever implementation could multithread the trawlers
        // by using a lazy, blocking collection for remaining.  This is a cool
        // idea but may add lots of complexity for no real performance gain.
        final List<Session> sessions = Lists.newArrayList();
        final Iterator<File> fileRoots = new FileWalkIterator(roots, pm);   // TODO: progress monitor
        final Iterator<Trawler> trawlers = new ArrayIterator<>(this.trawlers);
        final Collection<File> remaining = Sets.newLinkedHashSet();
        Trawler trawler = trawlers.next();
        if (trawler instanceof DicomTrawler) {
            ((DicomTrawler) trawler).setSeriesImportFilters(filters);
        }
        sessions.addAll(trawler.trawl(fileRoots, remaining, pm));
        while (trawlers.hasNext()) {
            final Collection<File> files = Lists.newArrayList(remaining);
            logger.trace("trawling {}", files);
            remaining.clear();
            trawler = trawlers.next();
            if (trawler instanceof DicomTrawler) {
                ((DicomTrawler) trawler).setSeriesImportFilters(filters);
            }
            sessions.addAll(trawler.trawl(files.iterator(), remaining, pm));
            if (null != pm && pm.isCanceled()) {
                logger.debug("user canceled file search");
                sessions.clear();
                return sessions;
            }
        }
        return sessions;
    }
}
