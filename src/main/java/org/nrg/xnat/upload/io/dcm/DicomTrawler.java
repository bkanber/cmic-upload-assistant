/*
 * upload-assistant: org.nrg.xnat.upload.io.dcm.DicomTrawler
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.io.dcm;

import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.io.StopTagInputHandler;
import org.nrg.dicomtools.utilities.DicomUtils;
import org.nrg.framework.io.EditProgressMonitor;
import org.nrg.xnat.upload.data.Session;
import org.nrg.xnat.upload.dcm.Series;
import org.nrg.xnat.upload.dcm.Study;
import org.nrg.xnat.upload.io.Trawler;
import org.nrg.xnat.upload.net.xnat.SeriesImportFilterApplicatorRetriever;
import org.nrg.xnat.upload.ui.SessionReviewPanel;
import org.nrg.xnat.upload.util.MapRegistry;
import org.nrg.xnat.upload.util.Registry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;

public final class DicomTrawler implements Trawler {

    public static final int MAX_TAG = Collections.max(new ArrayList<Integer>() {{
        add(Tag.AcquisitionNumber);
        add(Tag.SOPClassUID);
        add(Tag.SeriesDescription);
    }});

    public static final int APP_MAX_TAG = Collections.max(new ArrayList<Integer>() {{
        add(MAX_TAG);
        add(Series.MAX_TAG);
        add(SessionReviewPanel.MAX_TAG);
        add(Study.MAX_TAG);
        add(ZipSeriesUploader.MAX_TAG);
    }});

    private SeriesImportFilterApplicatorRetriever _filters;
    private final Logger logger = LoggerFactory.getLogger(DicomTrawler.class);

    /* (non-Javadoc)
     * @see Trawler#trawl(java.util.Iterator, java.util.Collection)
     */
    public Collection<Session> trawl(final Iterator<File> files, final Collection<File> remaining, EditProgressMonitor pm) {
        final Registry<Study> studies = new MapRegistry<>();

        PETMRSplitter manualSplitter = null;
        final String expectedModality = getSettings().getExpectedModality();
        if (StringUtils.isNotBlank(expectedModality)) {
            manualSplitter = new PETMRSplitter(expectedModality);
        }

        while (files.hasNext()) {
            if (null != pm && pm.isCanceled()) {
                return new ArrayList<>();
            }
            final File file = files.next();
            if (file.isFile()) {
                final DicomObject object;
                try {
                    object = DicomUtils.read(file, new StopTagInputHandler(APP_MAX_TAG + 1)); // We don't need anything higher than this tag.
                } catch (IOException e) {
                    remaining.add(file);
                    continue;
                } catch (Exception e) {
                    remaining.add(file);
                    continue;
                }

                assert null != object.getString(Tag.SOPClassUID);

                if (_filters != null && !StringUtils.equals(object.getString(Tag.MediaStorageSOPClassUID), UID.MediaStorageDirectoryStorage)) {
                    final String description = object.getString(Tag.SeriesDescription);
                    logger.debug("Found series import filters, testing series for inclusion/exclusion and suggested modality: {}", description);
                    if (_filters.shouldIncludeDicomObject(object) && (manualSplitter == null || manualSplitter.shouldIncludeDicomObject(object))) {
                        logger.info("DICOM object {} allowed in by import filters, including in session", description);
                        final Study study;
                        if (_filters.shouldSeparatePetMr() || (manualSplitter != null && manualSplitter.shouldIncludeDicomObject(object))) {
                            final String modality =  _filters != null ? _filters.findModality(object) : null;
                            study = StringUtils.isBlank(modality) ? studies.get(new Study(object)) : studies.get(new Study(object, modality));
                        } else {
                            study = studies.get(new Study(object));
                        }
                        study.getSeries(object, file);
                    } else {
                        logger.info("Series description {} was excluded by series import filter restrictions, excluding from session", description);
                    }
                }
            }
        }

        logger.debug("DicomTrawler complete with {} sessions", studies.size());

        return new ArrayList<Session>(studies.getAll());
    }

    public static class PETMRSplitter {
        private final String expectedModality;

        public PETMRSplitter(String object) {
            this.expectedModality = object;
        }

        public boolean shouldIncludeDicomObject(DicomObject object) {
            if (StringUtils.equals(expectedModality, "PET") || StringUtils.equals(expectedModality, "PT")) {
                String modality = object.getString(Tag.Modality);
                return !(modality != null && (modality.equals("MR")));
            } else if (StringUtils.equals(expectedModality, "MR")) {
                String modality = object.getString(Tag.Modality);
                return !(modality != null && (modality.equals("PET") || modality.equals("PT")));
            } else {
                return true;
            }
        }
    }

    public void setSeriesImportFilters(final SeriesImportFilterApplicatorRetriever filters) {
        _filters = filters;
    }
}
