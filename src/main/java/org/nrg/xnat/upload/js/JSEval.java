/*
 * upload-assistant: org.nrg.xnat.upload.js.JSEval
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.js;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;

public final class JSEval implements Callable<Object> {
    private final Object _context;
    private final Method _eval;
    private final String _code;

    public JSEval(final Object context, final String code) {
        _context = context;
        Method method;
        try {
            method = _context.getClass().getMethod("eval", String.class);
        } catch (NoSuchMethodException e) {
            method = null;
        }
        _eval = method;
        _code = code;
    }

    public Object call() {
        try {
            return _eval != null ? _eval.invoke(_context, _code) : null;
        } catch (IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }
}
