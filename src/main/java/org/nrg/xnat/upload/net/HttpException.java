/*
 * upload-assistant: org.nrg.xnat.upload.net.HttpException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net;

import java.io.IOException;

public class HttpException extends IOException {
    /**
     * Creates a new exception object containing relevant data for the error.
     *
     * @param responseCode    The response code that occurred.
     * @param responseMessage The message that accompanied the response.
     * @param text            The text for the exception.
     */
    public HttpException(final int responseCode, final String responseMessage, final String text) {
        this(responseCode, responseMessage, text, null);
    }

    /**
     * Creates a new exception object containing relevant data for the error.
     *
     * @param responseCode    The response code that occurred.
     * @param responseMessage The message that accompanied the response.
     * @param text            The text for the exception.
     * @param entity          The entity returned with the HTTP response.
     */
    public HttpException(final int responseCode, final String responseMessage, final String text, final String entity) {
        super(buildMessage(responseCode, responseMessage, text));
        _responseCode = responseCode;
        _entity = entity;
    }

    /**
     * Gets the code from the HTTP response.
     *
     * @return The response code.
     */
    public int getResponseCode() {
        return _responseCode;
    }

    /**
     * Gets the entity (or body) from the HTTP response if available and provided.
     *
     * @return The response entity.
     */
    public String getEntity() {
        return _entity;
    }

    private static String buildMessage(final int code, final String message, final String text) {
        final StringBuilder buffer = new StringBuilder("HTTP status ");
        buffer.append(code);
        if (null != message && !"".equals(message)) {
            buffer.append("(").append(message).append(")");
        }
        if (null != text && !"".equals(text)) {
            buffer.append(": ").append(text);
        }
        return buffer.toString();
    }

    private final int _responseCode;
    private final String _entity;
}
