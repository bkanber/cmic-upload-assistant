package org.nrg.xnat.upload.net;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Extracts aliases associated with the specified alias key from JSON objects.
 */
final class JSONAliasesExtractor implements JSONDecoder {
    JSONAliasesExtractor(final Map<String, String> aliases, final String aliasKey, final String idKey) {
        _aliases = aliases;
        _aliasKey = aliasKey;
        _idKey = idKey;
    }

    /**
     * Extracts values for aliases by first the configured alias key then by the configured ID key if the alias isn't
     * found.
     */
    public void decode(final JSONObject jsonObject) throws JSONException {
        _log.trace("decoding {} using {} -> {}", jsonObject, _aliasKey, _idKey);
        final String alias = jsonObject.has(_aliasKey) ? jsonObject.getString(_aliasKey) : null;
        final String id = jsonObject.has(_idKey) ? jsonObject.getString(_idKey) : null;
        if (StringUtils.isNotBlank(alias)) {
            _aliases.put(alias, StringUtils.defaultIfBlank(id, alias));
            _log.debug("Storing alias {} with value {}", alias, _aliases.get(alias));
        } else if (StringUtils.isNotBlank(id)) {
            _aliases.put(id, id);
            _log.debug("Storing alias by ID {} with value {}", id, _aliases.get(id));
        }
    }

    public Map<String, String> getAliases() {
        return _aliases;
    }

    private static final Logger _log = LoggerFactory.getLogger(JSONValuesExtractor.class);

    private final Map<String, String> _aliases;
    private final String              _aliasKey, _idKey;
}
