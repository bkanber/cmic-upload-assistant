/*
 * upload-assistant: org.nrg.xnat.upload.net.JSONValueMapExtractor
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Extracts values associated with the specified key from JSON objects.
 */
final class JSONValueMapExtractor implements JSONDecoder {
    JSONValueMapExtractor(final String... keys) {
        _keys.addAll(Arrays.asList(keys));
    }

    JSONValueMapExtractor(final Map<String, String> values, final String... keys) {
        this(keys);
        _values.putAll(values);
    }

    /**
     * Extracts the value corresponding to the configured key from the submitted JSON object.
     *
     * @param jsonObject The object from which to extract values.
     *
     * @throws JSONException When there's an error in the JSON.
     */
    public void decode(final JSONObject jsonObject) throws JSONException {
        for (final String key : _keys) {
            _log.trace("Extracting key {} from {}", key, jsonObject);
            if (jsonObject.has(key)) {
                final String value = jsonObject.getString(key);
                _values.put(key, value);
                _log.debug("Found value {} for key {}, now have {} values total.", StringUtils.defaultIfBlank(value, "<NULL>"), key, _values.size());
            }
        }
    }

    /**
     * Returns all the values extracted for the configured key.
     *
     * @return The values extracted for the configured key.
     */
    public Map<String, String> getValues() {
        return _values;
    }

    private static final Logger _log = LoggerFactory.getLogger(JSONValueMapExtractor.class);

    private final Map<String, String> _values = Maps.newHashMap();
    private final List<String>        _keys   = Lists.newArrayList();
}
