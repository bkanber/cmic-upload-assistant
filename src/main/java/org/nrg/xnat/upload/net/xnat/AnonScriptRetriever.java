/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.ProjectAnonScriptRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.net.xnat;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.xnat.upload.data.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;
import static org.nrg.xnat.upload.data.SessionVariableNames.PROJECT;

public class AnonScriptRetriever implements Callable<MizerContextWithScript> {

    public static final String PROJ_ANON_SCRIPT_ID = "id";
    public static final String PROJ_ANON_EDIT      = "edit";
    public static final String PROJ_ANON_SCRIPT    = "script";
    public static final String PROJ_ANON_STATUS    = "status";
    public static final String PROJ_ANON_VERSION   = "version";
    public static final String PROJ_ANON_CONTENTS  = "contents";

    public AnonScriptRetriever() {
        this(null);
    }

    public AnonScriptRetriever(final Project project) {
        _project = project;
    }

    @Override
    public MizerContextWithScript call() throws IOException, MizerException {
        return _project != null ? getProjectContext() : getSiteWideContext();
    }

    private MizerContextWithScript getSiteWideContext() throws IOException, MizerException {
        final Map<String, String> fetched = getRestServer().getValueMap(SITE_URL_TEMPLATE, PROJ_ANON_STATUS, PROJ_ANON_VERSION, PROJ_ANON_CONTENTS);
        if (fetched.size() == 0 || !fetched.containsKey(PROJ_ANON_STATUS) || !StringUtils.equalsAnyIgnoreCase(fetched.get(PROJ_ANON_STATUS), "enabled", "true")) {
            _log.trace("Site-wide anon script for {} is disabled", getCurrentXnatServer().getAddress());
            return null;
        }

        final String script = fetched.get(PROJ_ANON_CONTENTS);
        if (StringUtils.isBlank(script)) {
            return null;
        }

        return new MizerContextWithScript() {{
            setScriptId(Long.parseLong(StringUtils.defaultIfBlank(fetched.get(PROJ_ANON_VERSION), "0")));
            setScript(script);
        }};
    }

    private MizerContextWithScript getProjectContext() throws IOException, MizerException {
        final Map<String, String> values = new HashMap<>();
        values.put(PROJECT, _project.toString());
        values.put(RESOURCE, PROJ_ANON_STATUS);

        final Map<String, String> fetched = getRestServer().getValueMap(new StrSubstitutor(values).replace(PROJECT_URL_TEMPLATE), PROJ_ANON_SCRIPT_ID, PROJ_ANON_EDIT);

        if (fetched.size() == 0 || !fetched.containsKey(PROJ_ANON_EDIT) || !StringUtils.equals("true", fetched.get(PROJ_ANON_EDIT))) {
            _log.trace("project {} anon script is disabled", _project);
            return null;
        }

        final MizerContextWithScript context = new MizerContextWithScript();
        context.setScriptId(fetched.containsKey(PROJ_ANON_SCRIPT_ID) ? Long.parseLong(fetched.get(PROJ_ANON_SCRIPT_ID)) : 0L);

        values.put(RESOURCE, PROJ_ANON_SCRIPT);
        final Collection<?> scripts = getRestServer().getValues(new StrSubstitutor(values).replace(PROJECT_URL_TEMPLATE), PROJ_ANON_SCRIPT);
        _log.trace("project {} script text: {}", _project, scripts);
        for (final Object script : scripts) {
            if (null == script) {
                continue;
            }
            context.setScript(script.toString());
            _log.trace("project {} script {} enabled and retrieved", _project, context.getScriptId());
            return context;
        }
        _log.trace("project {} script {} was enabled but couldn't find a valid script", _project, context.getScriptId());
        return null;
    }

    private static final String SITE_URL_TEMPLATE    = "/data/config/anon/script?format=json";
    private static final String PROJECT_URL_TEMPLATE = "/data/config/edit/projects/${project}/image/dicom/${resource}";
    private static final String RESOURCE             = "resource";

    private final static Logger _log = LoggerFactory.getLogger(AnonScriptRetriever.class);

    private final Project _project;
}
