/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.BaseScriptApplicatorRetriever
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net.xnat;

import org.nrg.xnat.upload.net.HttpException;
import org.nrg.xnat.upload.net.HttpURLConnectionProcessor;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

class BaseScriptApplicatorRetriever<ApplicatorT> implements Callable<ApplicatorT> {
    public interface ScriptApplicatorFactory<A> {
        A createScriptApplicator(final InputStream stream) throws Exception;
    }

    BaseScriptApplicatorRetriever(final ScriptApplicatorFactory<ApplicatorT> factory,
                                  final String path) {
        _factory = factory;
        _path = path;
    }

    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    public final ApplicatorT call() throws Exception {
        final ConnectionProcessor<ApplicatorT> processor = new ConnectionProcessor<>(_factory);
        try {
            getRestServer().doGet(_path, processor);
            return processor.getApplicator();
        } catch (HttpException e) {
            // This is OK, there was just no script to be found.
            if (e.getResponseCode() == 404) {
                return null;
            }
            throw e;
        }
    }

    public static final class ConnectionProcessor<ApplicatorT> implements HttpURLConnectionProcessor {
        public ConnectionProcessor(final ScriptApplicatorFactory<ApplicatorT> factory) {
            _factory = factory;
        }

        /*
         * (non-Javadoc)
         * @see HttpURLConnectionProcessor#prepare(java.net.HttpURLConnection)
         */
        public void prepare(final HttpURLConnection c) {
        }

        /*
         * (non-Javadoc)
         * @see HttpURLConnectionProcessor#process(java.net.HttpURLConnection)
         */
        public void process(final HttpURLConnection c) throws Exception {
            _applicator = _factory.createScriptApplicator(c.getInputStream());
        }

        public ApplicatorT getApplicator() {
            return _applicator;
        }

        private final ScriptApplicatorFactory<ApplicatorT> _factory;
        private ApplicatorT _applicator = null;
    }

    private final String                               _path;
    private final ScriptApplicatorFactory<ApplicatorT> _factory;
}
