package org.nrg.xnat.upload.net.xnat;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xnat.upload.net.HttpException;
import org.nrg.xnat.upload.net.StringResponseProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public class NamingConventionRetriever implements Callable<String> {
    public NamingConventionRetriever(final String project) {
        _projectPath = String.format(URL_TEMPLATE_PROJECT, project);
    }

    public String call() {
        try {
            // check to see if we got back a status page instead of the setting
            // if so, there's no project specific list, so just get the site wide setting
            try {
                final String convention = getNamingConvention(_projectPath);
                if (StringUtils.equals(convention, "DEFAULT")) {
                    logger.debug("project specific naming convention is DEFAULT");
                    throw new Exception("Use the site-wide script");
                } else if (StringUtils.isBlank(convention)) {
                    logger.debug("project specific naming convention is empty");
                    return null;
                } else {
                    logger.debug("project specific naming convention is " + convention);
                    return StringUtils.remove(convention, '\n');
                }
            } catch (Throwable t) {
                logger.debug("failed to retrieve project specific naming convention, querying site-wide default");
                final String convention = getNamingConvention(URL_TEMPLATE_SITE);
                if (StringUtils.equals(convention, "DEFAULT")) {
                    logger.debug("site-wide naming convention is DEFAULT");
                    return null;
                } else if (StringUtils.isBlank(convention)) {
                    logger.debug("site-wide naming convention is empty");
                    return null;
                } else {
                    logger.debug("site-wide naming convention is " + convention);
                    return StringUtils.remove(convention, '\n');
                }
            }
        } catch (Throwable t) {
            logger.error("", t);
            //default to not required
            return null;
        }
    }

    private String getNamingConvention(final String path) throws Exception {
        try {
            final StringResponseProcessor processor = new StringResponseProcessor();
            getRestServer().doGet(path, processor);
            JSONObject json = new JSONObject(processor.toString());
            return json.getString("pattern");
        } catch (JSONException e) {
            logger.error("An error occurred processing the retrieved JSON", e);
            return null;
        } catch (HttpException e) {
            if (e.getResponseCode() == 404) {
                logger.debug("no naming convention configured for the path {}", path);
                return null;
            } else {
                logger.error("An unknown error occurred accessing the resource at path " + path, e);
                return null;
            }
        }
    }

    private static final String URL_TEMPLATE_SITE    = "/data/config/cr_naming/template?contents=true";
    private static final String URL_TEMPLATE_PROJECT = "/data/projects/%s/config/cr_naming/template?contents=true";
    private static final Logger logger               = LoggerFactory.getLogger(NamingConventionRetriever.class);

    private final String _projectPath;
}
