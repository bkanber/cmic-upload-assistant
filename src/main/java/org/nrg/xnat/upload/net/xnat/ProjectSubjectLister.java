/*
 * upload-assistant: org.nrg.xnat.upload.net.xnat.ProjectSubjectLister
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.net.xnat;

import org.json.JSONException;
import org.nrg.xnat.upload.net.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Callable;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class ProjectSubjectLister implements Callable<Map<String, String>> {
    public ProjectSubjectLister(final String project) {
        _uri = String.format(URL_TEMPLATE, project);
    }

    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    public Map<String, String> call() throws IOException, JSONException {
        try {
            return getRestServer().getAliases(_uri);
        } catch (HttpException e) {
            if (e.getResponseCode() == 404) {
                _log.warn("Got a 404 calling {}. This may mean there are no subjects, but it may also mean something went wrong on the server.", _uri);
            } else {
                throw e;
            }
        }
        return Collections.emptyMap();
    }

    private static final String URL_TEMPLATE = "/data/projects/%s/subjects?format=json";

    private static final Logger _log = LoggerFactory.getLogger(ProjectSubjectLister.class);

    private final String _uri;
}
