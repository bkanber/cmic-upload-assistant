/*
 * org.nrg.upload.ui.AssignSessionVariablesPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 2/11/14 4:28 PM
 */
package org.nrg.xnat.upload.ui;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.commons.lang3.time.DateUtils;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardController;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;
import org.nrg.dcm.SOPModel;
import org.nrg.dicom.mizer.exceptions.MultipleInitializationException;
import org.nrg.dicom.mizer.service.MizerContext;
import org.nrg.dicom.mizer.service.impl.MizerContextWithScript;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.framework.constants.PrearchiveCode;
import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.data.*;
import org.nrg.xnat.upload.data.SessionVariable.InvalidValueException;
import org.nrg.xnat.upload.dcm.DicomSessionVariable;
import org.nrg.xnat.upload.dcm.Study;
import org.nrg.xnat.upload.ecat.EcatSession;
import org.nrg.xnat.upload.ecat.EcatSessionVariable;
import org.nrg.xnat.upload.ecat.SessionVariableValue;
import org.nrg.xnat.upload.net.xnat.PETTracerRetriever;
import org.nrg.xnat.upload.net.xnat.ProjectSession;
import org.nrg.xnat.upload.net.xnat.SessionScanCount;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;
import static org.nrg.xnat.upload.data.SessionVariableNames.*;

@SuppressWarnings("Duplicates")
public final class AssignSessionVariablesPage extends WizardPage implements SessionVariableConsumer, PropertyChangeListener, ActionListener {
    /**
     * Gets the description of this step for the wizard framework.
     *
     * @return The step description.
     */
    public static String getDescription() {
        return Messages.getPageTitle(AssignSessionVariablesPage.class);
    }

    /**
     * Default constructor.
     */
    public AssignSessionVariablesPage() {
        setLayout(new BorderLayout());
        setLongDescription(Messages.getPageDescription(AssignSessionVariablesPage.class));
    }

    /**
     * Implementation of the method.
     *
     * @see SessionVariableConsumer#update(SessionVariable, boolean)
     */
    @Override
    public void update(final SessionVariable v, final boolean isValidValue) {
        logger.trace("{} updated to {}", v, isValidValue ? "valid" : "invalid");
        if (isValidValue) {
            invalid.remove(v);
        } else {
            invalid.add(v);
            setProblem(v.getValueMessage());
        }
        setProblem(validateContents(null, null));
    }

    /**
     * Verifies whether or not user wants to proceed in the case of a verification date mismatch.
     *
     * @param stepName The name of the current step.
     * @param settings Any settings for the current step.
     * @param wizard   The current wizard.
     *
     * @return Returns <b>WizardPanelNavResult.REMAIN_ON_PAGE</b> if the user doesn't confirm,
     * <b>WizardPanelNavResult.PROCEED</b> otherwise.
     */
    @Override
    public WizardPanelNavResult allowFinish(String stepName, @SuppressWarnings("rawtypes") Map settings, Wizard wizard) {
        final WizardPanelNavResult result = isOkToProceed() ? WizardPanelNavResult.PROCEED : WizardPanelNavResult.REMAIN_ON_PAGE;
        if (result.equals(WizardPanelNavResult.PROCEED)) {
            _manager.disableEditors();
        }
        return result;
    }

    /**
     * Implements the {@link PropertyChangeListener#propertyChange(PropertyChangeEvent)} method. This is used to
     * prompt the wizard framework to take notice when something has changed on the page.
     *
     * @param event The property change event.
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {
        userInputReceived(this, event);
    }

    @Override
    public void actionPerformed(final ActionEvent event) {
        _log.info("Got event {}", event.toString());
        userInputReceived(this, event);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void recycle() {
        removeAll();
        invalid.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void renderingPage() {
        initialize();

        final String expectedModality = getSettings().getExpectedModality();
        final Set<String> modalities = session.getModalities();

        if (StringUtils.isNotBlank(expectedModality) &&
            (!modalities.contains(expectedModality) ||
             (("PT".equalsIgnoreCase(expectedModality) || "PET".equalsIgnoreCase(expectedModality) || "PETMR".equalsIgnoreCase(expectedModality)) &&
              (!(modalities.contains("PT") || modalities.contains("PET")))))) {
            JOptionPane.showMessageDialog(this,
                                          Messages.getFormattedMessage("assignsessionvariablespage.unexpected.modality.message"),
                                          Messages.getMessage("assignsessionvariablespage.unexpected.modality.title"),
                                          JOptionPane.WARNING_MESSAGE);
        }
        if (sessionDate == null && confirmedDate != null && confirmedDate != Constants.NO_DATE) {
            JOptionPane.showMessageDialog(this,
                                          Messages.getFormattedMessage("assignsessionvariablespage.null.session.date.message"),
                                          Messages.getMessage("assignsessionvariablespage.null.session.date.title"),
                                          JOptionPane.WARNING_MESSAGE);
        } else if (!isSessionDateOk(confirmedDate, sessionDate, session.getTimeZone())) {
            JOptionPane.showMessageDialog(this,
                                          Messages.getFormattedMessage("assignsessionvariablespage.invalid.session.date.message"),
                                          Messages.getMessage("assignsessionvariablespage.invalid.session.date.title"),
                                          JOptionPane.ERROR_MESSAGE);
        } else {
            add(addContent(new JPanel(new GridBagLayout())), BorderLayout.CENTER);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String validateContents(final Component component, final Object event) {
        if (!isSessionDateOk(confirmedDate, sessionDate, session.getTimeZone())) {
            // We have to go through this whole rigamarole to deal with the Java Date class's inadequacies and general crappiness.
            // The year being set by dcm4che seems to absolute, i.e. setting 2012 when it really means 2012. The nerve. Instead,
            // according to Java, it should be setting the offset from 1900, i.e. 112, when it means 2012. But we get what we get.
            // So, check for a year greater than 1900 and, if found, offset by -1900 and hope like crazy that that's the right thing.
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(confirmedDate);
            //            int year = calendar.get(Calendar.YEAR);
            //            if (year > 1900) {
            //                calendar.set(Calendar.YEAR, year - 1900);
            //            }
            return "Click the Previous button and select a session with scan date of " + FORMATTER.format(calendar.getTime());
        }

        // If verifyDate is empty, we won't allow user to proceed, but we won't do any other validation.
        if (component != null && component instanceof JTextField) {
            final String candidate = StringUtils.defaultIfBlank(getSettings().getPredefinedSessionLabel(), ((JTextField) component).getText());
            final boolean stopForDuplicateSessionId = stopForDuplicateSessionId(candidate, false);
            if (stopForDuplicateSessionId) {
                return "You must either select another session, change the session ID, or indicate how you want to handle the duplicate session ID for your auto-archiving project.";
            }
        }

        final SortedSet<String> names = Sets.newTreeSet();
        for (final SessionVariable variable : invalid) {
            names.add(variable.getName());
        }
        if (names.size() > 0) {
            return "Some fields have invalid values: " + Joiner.on(", ").join(names);
        }
        return null;
    }

    /**
     * Initializes all incoming data from the wizard's data map.
     */
    private void initialize() {
        setForwardNavigationMode(WizardController.MODE_CAN_FINISH);

        project = getCurrentXnatServer().getCurrentProject();
        subject = getSettings().getCurrentSubject();
        session = getSettings().getCurrentSession();
        sessionDate = session.getDateTime();
        warnOnDupeSessionLabels = getSettings().isWarnOnDupeSessionLabels();
        allowOverwriteOnDupeSessionLabels = getSettings().isAllowOverwriteOnDupeSessionLabels();
        allowAppendOnDupeSessionLabels = getSettings().isAllowAppendOnDupeSessionLabels();

        // Get the confirmed date that the user set in ConfirmSessionDatePage.
        confirmedDate = getSettings().getCurrentSessionDate();

        // If that confirmed date is NO_DATE (i.e. no date specified or unknown)...
        if (confirmedDate == Constants.NO_DATE) {
            // Then we're ignoring the session date. Set the two to the same value and set unsetDate to true.
            sessionDate = confirmedDate;
            unsetDate = true;
        }

        _siteWideAnonScript = getCurrentXnatServer().getSiteWideAnonScript();
    }

    private JPanel addContent(final JPanel panel) {
        logger.trace("adding content");

        try {
            isAutoArchiving = project.getPrearchiveCode() != PrearchiveCode.Manual;
        } catch (Exception exception) {
            logger.warn("Error when retrieving project prearchive code", exception);
        }

        final Map<String, SessionVariable> predefinedMappings = Maps.newLinkedHashMap();
        predefinedMappings.put(PROJECT, new AssignedSessionVariable(PROJECT, project.toString()).fixValue());
        final SessionVariable vSubject = new AssignedSessionVariable(SUBJECT, subject.toString()).fixValue();
        predefinedMappings.put(SUBJECT, vSubject);

        final UploadAssistantSettings settings = getSettings();
        final Project project = getCurrentXnatServer().getCurrentProject();

        final Set<String> modalities = session.getModalities();

        final Map<String, SessionVariable> variables;
        if (session instanceof Study) {
            final List<MizerContext> scripts = Lists.newArrayList();
            final MizerContext siteWideAnonScript = getSiteWideAnonScript();
            if (siteWideAnonScript != null) {
                scripts.add(siteWideAnonScript);
            }
            final MizerContext projectAnonScript = project.getProjectAnonScript();
            if (projectAnonScript != null) {
                scripts.add(projectAnonScript);
            }
            variables = scripts.size() > 0 ? getVariablesMap(session.getVariables(scripts)) : Maps.<String, SessionVariable>newHashMap();
        } else if (session instanceof EcatSession) {
            variables = getVariablesMap(session.getVariables(project));
        } else {
            throw new RuntimeException("I don't know how to handle a session of type " + session.getClass().getName());
        }

        // Strip project and subject out of the List: these are immutable and we'll
        // display them separately.  Also check whether session has been defined;
        // we'll use the existing variable if so, or add a new one if not.
        SessionVariable modalityLabel = null;
        final List<String> removals = Lists.newArrayList();
        for (final String name : variables.keySet()) {
            final SessionVariable variable = variables.get(name);
            if (predefinedMappings.containsKey(name)) {
                final SessionVariable predef = predefinedMappings.get(name);
                logger.trace("found predefined variable {} in script", variable);
                try {
                    variable.fixValue(predef.getValue().asString());
                } catch (InvalidValueException e) {
                    throw new RuntimeException(e);
                }
                removals.add(name);
                predefinedMappings.put(name, variable);
            } else if (MODALITY_LABEL.equals(name)) {
                modalityLabel = variable;   // Process this later
            } else if (TRACER.equals(name)) {
                logger.trace("found session variable {}, will shadow", variable);
                // tracer is special and PET-only
                final String field = variable.getExportField();
                if (!Strings.isNullOrEmpty(field) && !TRACER_PATH.equals(field)) {
                    logger.error("script variable {} has unexpected export path {}, replacing with " + TRACER, variable, field);
                }
                tracer = variable;  // Process this later
                removals.add(name);
            }
        }

        // Any variables that were moved to predefs or other places should be removed from the variable map.
        for (final String removal : removals) {
            variables.remove(removal);
        }

        // modality = PET indicates an ECAT data set, so obviously that's PET.
        final String leadModality = modalities.contains("PET") ? "PET" : SOPModel.getLeadModality(modalities);
        final ExcludingValueValidator excluder = getSessionExcluder(project);

        // If this is a PET study, allow the user to specify a tracer.
        final boolean hasPredefinedSessionLabel = StringUtils.isNotBlank(settings.getPredefinedSessionLabel());
        final boolean hasPETNoLabel = !hasPredefinedSessionLabel && (modalities.contains("PET") || modalities.contains("PT"));
        final boolean hasVisitAndModality = settings.getSelectedVisit() != null && settings.getSelectedVisitModality() != null;

        if (hasPETNoLabel) {
            logger.trace("Found PET in session without predefined label");
            final Set<String> tracers = Sets.newLinkedHashSet();
            try {
                tracers.addAll(this.project.getPETTracers());
            } catch (Throwable t) {
                logger.error("error retrieving PET tracers", t);
                tracers.clear();
                tracers.addAll(PETTracerRetriever.getDefaultTracers());
            }
            if (tracer == null) {
                tracer = new EnumeratedSessionVariable(TRACER, TRACER_PATH, tracers, true, true);
            } else {
                ((EnumeratedSessionVariable) tracer).setItems(tracers);
            }
        }

        SessionVariable vSession = null;
        if (hasPredefinedSessionLabel) {
            predefinedMappings.put(SESSION, vSession = new AssignedSessionVariable(SESSION, settings.getPredefinedSessionLabel()));
        } else if (hasVisitAndModality) {
            try {
                predefinedMappings.put(SESSION, vSession = buildSessionVariableFromVisit());
            } catch (InterruptedException | ExecutionException e) {
                logger.error("An error occurred trying to retrieve the visit and modality", e);
            }
        } else if (variables.containsKey(SESSION)) {
            vSession = variables.get(SESSION);
        }

        if (null != modalityLabel) {
            if (modalityLabel instanceof DicomSessionVariable) {
                final DicomSessionVariable variable = (DicomSessionVariable) modalityLabel;
                try {
                    if (null == tracer) {
                        logger.trace("setting (hidden) modality label from lead modality {}", leadModality);
                        variable.setInitialValue(leadModality);
                    } else {
                        logger.trace("setting (hidden) modality label from tracer variable {}", tracer);
                        variable.setInitialValue(tracer.getValue());
                        tracer.addListener(variable);
                    }
                } catch (MultipleInitializationException e) {
                    _log.warn(e.getMessage());
                }
                variable.setIsHidden(true);
            } else if (modalityLabel instanceof EcatSessionVariable) {
                final EcatSessionVariable variable = (EcatSessionVariable) modalityLabel;
                try {
                    if (null == tracer) {
                        variable.setInitialValue(new ConstantValue(leadModality));
                    } else {
                        variable.setInitialValue(new SessionVariableValue(tracer));
                    }
                    variable.setIsHidden(true);
                } catch (MultipleInitializationException exception) {
                    logger.debug("Got MultipleInitializationException", exception);
                }
            }
            variables.put(MODALITY_LABEL, modalityLabel);
        }

        // If no initial value was provided for session, construct one in the format
        // {subject}_{modality}{index}, where index is the smallest positive integer
        // that results in a session label not already defined in this project.
        // TODO: This could be:
        // final AbstractSessionVariable defaultSessionLabel;
        final IndexedDependentSessionVariable defaultSessionLabel;
        if (hasPETNoLabel) {
            // use the tracer instead of the modality string for PET
            assert null != tracer;
            logger.trace("building indexed tracer default session label from {}", tracer);
            defaultSessionLabel = new IndexedDependentSessionVariable(SESSION, tracer, subject + "_%s%d", excluder);
        } else {
            logger.trace("building indexed modality default session label from {}", leadModality);
            defaultSessionLabel = new IndexedDependentSessionVariable(SESSION, vSubject, "%s_" + leadModality + "%d", excluder);
        }

        if (null == vSession) {
            variables.put(SESSION, vSession = defaultSessionLabel);
        } else if (vSession.isMutable() && !LabelValueValidator.getInstance().isValid(vSession.getValue())) {
            // The existing session variable doesn't have a useful initial value,
            // so shadow it with the default label format.
            try {
                vSession.setValue(defaultSessionLabel.getValue());
            } catch (InvalidValueException e) {
                logger.error("unable to set shadowed session variable", e);
            }

            defaultSessionLabel.addShadow(vSession);
            predefinedMappings.put(SESSION, defaultSessionLabel);
        }

        settings.setSessionVariables(variables);
        logger.trace("initialized session variables: {}", variables);

        logger.trace("starting upload for {}", predefinedMappings);

        if (vSession.isMutable()) {
            // This handles JIRA XNAT-985: upon returning to the page, a new tracer control is created, but the session
            // identifier control still has a reference to the old tracer control, breaking the dependency between the controls.
            // We also have to check for tracer != null to keep from overwriting other dependency types, especially when
            // it uses the subject as the dependency.
            if (tracer != null && vSession instanceof IndexedDependentSessionVariable) {
                IndexedDependentSessionVariable indexed = (IndexedDependentSessionVariable) vSession;
                if (indexed.getDependency() == null || !indexed.getDependency().equals(tracer)) {
                    indexed.setDependency(tracer);
                }
            }

            vSession.addValidator(LabelValueValidator.getInstance());
        }

        if (hasPETNoLabel) {
            assert null != tracer;
            if (!variables.containsValue(tracer)) {
                variables.put(tracer.getName(), tracer);
            }
        }

        addSessionIdentifiers(panel, predefinedMappings, variables);
        settings.setSessionLabel(vSession);

        return panel;
    }

    private MizerContext getSiteWideAnonScript() {
        try {
            return _siteWideAnonScript.get();
        } catch (InterruptedException | ExecutionException e) {
            logger.error("An error occurred trying to retrieve the site-wide anon script. Uploading can't continue, since this might introduce PHI to the system.", e);
            throw new RuntimeException(e);
        }
    }

    private Map<String, SessionVariable> getVariablesMap(final List<SessionVariable> variables) {
        final Map<String, SessionVariable> map = Maps.newHashMap();
        for (final SessionVariable variable : variables) {
            map.put(variable.getName(), variable);
        }
        return map;
    }

    /**
     * Added for CR's visit structure and session naming structure
     *
     * @return Returns the session variable built from the visit data.
     *
     * @throws ExecutionException   When an error occurs executing code calling the XNAT server.
     * @throws InterruptedException When the call to the XNAT server is interrupted for some reason.
     */
    private SessionVariable buildSessionVariableFromVisit() throws ExecutionException, InterruptedException {
        Map<String, String> substitutions = Maps.newHashMap();
        substitutions.put("PROJECT", project.toString());
        substitutions.put("SUBJECTID", subject.getId());//added for backwards compatibility
        substitutions.put("SUBJECT_ID", subject.getId());
        substitutions.put("SUBJECT_LABEL", subject.getLabel());
        substitutions.put("SUBJECTLABEL", subject.getLabel());//added for backwards compatibility
        substitutions.put("VISIT", getSettings().getSelectedVisit().getCode());
        substitutions.put("MOD", getSettings().getSelectedVisitModality().getLabel());
        final Date sessionDate = getSettings().getScanDate();
        if (sessionDate != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
            substitutions.put("SESSION_DATE", formatter.format(sessionDate));

            formatter = new SimpleDateFormat("HH");
            substitutions.put("HOUR", formatter.format(sessionDate));

            formatter = new SimpleDateFormat("mm");
            substitutions.put("MINUTE", formatter.format(sessionDate));
        }

        final String label = StringUtils.defaultIfBlank(project.getSessionNamingConvention(), "{SUBJECT_LABEL}_{VISIT}_{MOD}");
        return new AssignedSessionVariable(SESSION, StrSubstitutor.replace(label, substitutions, "{", "}"));
    }

    /**
     * Indicates whether a duplicate session ID has been identified for an auto-archived project and if the user has
     * indicated how to handle it.
     *
     * @return <b>false</b> if the project is not set to auto-archive, the selected session ID is not a duplicate of an
     * existing session ID, or the user has indicated whether the duplicate session should be appended to or
     * overwrite the existing session with the duplicate ID. In the case where a duplicate session ID is found
     * for an auto-archiving project and the user selects <b>Cancel</b> on the option dialog, this method
     * returns <b>true</b>.
     */
    private boolean stopForDuplicateSessionId(String candidate, boolean showDialog) {
        // Notify about duplicate session IDs when warning flag is set or the project is auto-archived.
        if (warnOnDupeSessionLabels || isAutoArchiving) {
            try {
                Map<String, String> labels = project.getSessionLabels().toMap();
                if (StringUtils.isNotBlank(candidate) && labels.containsKey(candidate)) {
                    if (showDialog) {
                        if (allowOverwriteOnDupeSessionLabels) {
                            final List<String> options = Messages.getMessages("assignsessionvariablespage.dup.session.id.options.overwrite", "assignsessionvariablespage.dup.session.id.options.append", "assignsessionvariablespage.dup.session.id.options.new");
                            int selected = JOptionPane.showOptionDialog(this,
                                                                        Messages.getFormattedMessage("assignsessionvariablespage.dup.session.id.message.with.overwrite"),
                                                                        Messages.getMessage("assignsessionvariablespage.dup.session.id.title"),
                                                                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                                                                        options.toArray(new String[3]), options.get(2));
                            switch (selected) {
                                case JOptionPane.YES_OPTION:
                                    getSettings().setPrearchiveCodeOverride(PrearchiveCode.AutoArchiveOverwrite);
                                    break;
                                case JOptionPane.NO_OPTION:
                                    getSettings().setPrearchiveCodeOverride(PrearchiveCode.AutoArchive);
                                    break;
                                case JOptionPane.CLOSED_OPTION:
                                case JOptionPane.CANCEL_OPTION:
                                    return true;
                            }
                        } else if (allowAppendOnDupeSessionLabels) {
                            final List<String> options = Messages.getMessages("assignsessionvariablespage.dup.session.id.options.append", "assignsessionvariablespage.dup.session.id.options.new");
                            int selected = JOptionPane.showOptionDialog(this,
                                                                        Messages.getFormattedMessage("assignsessionvariablespage.dup.session.id.message"),
                                                                        Messages.getMessage("assignsessionvariablespage.dup.session.id.title"),
                                                                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                                                                        options.toArray(new String[2]), options.get(1));
                            switch (selected) {
                                case JOptionPane.YES_OPTION:
                                    getSettings().setPrearchiveCodeOverride(PrearchiveCode.AutoArchive);
                                    break;
                                case JOptionPane.NO_OPTION:
                                case JOptionPane.CLOSED_OPTION:
                                    return true;
                            }
                        } else {
                            JOptionPane.showMessageDialog(this,
                                                          Messages.getFormattedMessage("assignsessionvariablespage.dup.session.id.wo.append.message"),
                                                          Messages.getMessage("assignsessionvariablespage.dup.session.id.title"),
                                                          JOptionPane.WARNING_MESSAGE);
                            return true;
                        }
                    } else {
                        return true;
                    }
                }
            } catch (Exception exception) {
                logger.warn("Error when retrieving project session labels for project", exception);
            }
        }

        return false;
    }

    private void addSessionIdentifiers(JPanel panel, Map<String, SessionVariable> predefined, Map<String, SessionVariable> variables) {
        for (final SessionVariable variable : predefined.values()) {
            panel.add(new JLabel(variable.getLabel()), VariableAssignmentManager.labelConstraint);
            panel.add(new JLabel(variable.getValue().asString()), VariableAssignmentManager.valueConstraint);
        }

        if (variables.size() > 0) {
            panel.add(new JLabel("<html><b>Set session identifiers:</b></html>"), SPANNING);
            panel.add(new JLabel(), VariableAssignmentManager.messageConstraint);
        }
        final List<SessionVariable> values = Lists.newArrayList(variables.values());
        Collections.sort(values, VAR_COMPARATOR);
        _manager = new VariableAssignmentManager(panel, values, this);
    }

    private ExcludingValueValidator getSessionExcluder(final Project project) {
        Set<String> labels;
        try {
            labels = project.getSessionLabels().toMap().keySet();
        } catch (Exception e) {
            labels = Collections.emptySet();
        }
        return new ExcludingValueValidator(labels, "Project already contains a session named %s.", true);
    }

    private boolean isOkToProceed() {
        // If our dates are bad, we can't go forward.
        if (!(sessionDate != null && isSessionDateOk(confirmedDate, sessionDate, session.getTimeZone()))) {
            return false;
        }

        // If we don't have a value for the session label at all, we can't go forward.
        final SessionVariable sessionLabelVariable = getSettings().getSessionLabel();
        if (sessionLabelVariable == null) {
            return false;
        }
        final String sessionLabel = sessionLabelVariable.getValue().asString();
        if (!LabelValueValidator.getInstance().isValid(sessionLabel)) {
            return false;
        }

        try {
            //block reupload of locked sessions
            for (ProjectSession ps : project.getSessionLabels().getSessions()) {
                if (StringUtils.equals(sessionLabel, ps.getLabel()) && !StringUtils.equals("active", ps.getStatus())) {
                    JOptionPane.showMessageDialog(this,
                                                  Messages.getFormattedMessage("assignsessionvariablespage.locked.session.id.wo.append.message"),
                                                  Messages.getMessage("assignsessionvariablespage.dup.session.id.title"),
                                                  JOptionPane.WARNING_MESSAGE);
                    return false;
                }
            }
        } catch (Exception exception) {
            logger.warn("Error when retrieving project session labels for project", exception);
        }

        try {
            if (project.getSessionMergingSettings().contains("16")) {
                //scan overwrite is not allowed
                //confirm if this session exists with scans
                if (hasScans(project.toString(), sessionLabel)) {
                    JOptionPane.showMessageDialog(this,
                                                  Messages.getFormattedMessage("assignsessionvariablespage.scan.conflict.session.id.wo.append.message"),
                                                  Messages.getMessage("assignsessionvariablespage.dup.session.id.title"),
                                                  JOptionPane.WARNING_MESSAGE);
                    return false;
                }
            } else {
                // If we have a duplicate session label we have to check to see if they've
                // selected an appropriate autoarchive setting. If so, we can let them proceed.
                try {
                    return !stopForDuplicateSessionId(sessionLabel, true) && project.getPrearchiveCode() != null;
                } catch (InterruptedException | ExecutionException e) {
                    logger.error("An error occurred retrieving the project " + project.toString() + " prearchive code.", e);
                    return false;
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            logger.error("An error occurred retrieving the project " + project.toString() + " session merging settings.", e);
        }

        return true;
    }

    private boolean hasScans(String project, String label) {
        SessionScanCount scanCounts = new SessionScanCount(project, label);
        return (scanCounts.call() > 0);
    }

    //If d1's hours and minutes are both == 0 we assume we're testing
    //to see if both dates occurred on the same day.  If there is data
    //in the hours and minutes, we return true if both dates are within
    // a 61 minute window of each other.
    private boolean isSessionDateOk(final Date confirmed, final Date session, final TimeZone sessionTimeZone) {
        // if no date was passed into the application, confirmedDate has already been set to equal sessionDate
        if (unsetDate) {
            return true;
        } else {
            final Calendar calendar = Calendar.getInstance();
            calendar.setTime(confirmed);
            final int hour = calendar.get(Calendar.HOUR_OF_DAY);
            final int minute = calendar.get(Calendar.MINUTE);
            if (hour == 0 && minute == 0) {
                return DateUtils.isSameDay(confirmed, session);
            } else {
                //check the 61 minute window
                if (sessionTimeZone != null) {
                    //if the session has a time zone, we have to deal with conversion...
                    final Calendar sessionCal = Calendar.getInstance();
                    sessionCal.setTimeZone(sessionTimeZone);
                    sessionCal.setTime(session);
                    int sessionHour = sessionCal.get(Calendar.HOUR_OF_DAY);
                    //make sure the date is the same day (within 24 hours) and assure the hour is within 1 hour.
                    return Math.abs(confirmed.getTime() - session.getTime()) < 86400000 && Math.abs(hour - sessionHour) <= 2;
                } else {
                    return Math.abs(confirmed.getTime() - session.getTime()) < 7320000;
                }
            }
        }
    }

    private static final Logger _log = LoggerFactory.getLogger(AssignSessionVariablesPage.class);

    /**
     * A comparator that mostly does standard alphabetical sorting, with the exception of variables with the name {@link
     * SessionVariableNames#SESSION}, which are always sorted to the top.
     */
    private static final Comparator<? super SessionVariable> VAR_COMPARATOR = new Comparator<SessionVariable>() {
        @Override
        public int compare(final SessionVariable first, final SessionVariable second) {
            final String firstName = first.getName();
            final String secondName = second.getName();
            if (StringUtils.equals(firstName, secondName)) {
                return 0;
            }
            if (StringUtils.equals(SESSION, firstName)) {
                return 1;
            }
            if (StringUtils.equals(SESSION, secondName)) {
                return -1;
            }
            return StringUtils.compare(firstName, secondName);
        }
    };

    private static final GridBagConstraints SPANNING  = new GridBagConstraints() {
        private static final long serialVersionUID = 5114328188210435952L;

        {
            gridx = 0;
            gridwidth = 2;
            insets = new Insets(8, 0, 0, 0);
        }
    };
    private static final DateFormat         FORMATTER = new SimpleDateFormat("M/d/yyyy HH:mm");
    private static final Logger             logger    = LoggerFactory.getLogger(AssignSessionVariablesPage.class);

    private final Set<SessionVariable> invalid = Sets.newLinkedHashSet();

    private Future<MizerContextWithScript> _siteWideAnonScript;

    private Project                   project;
    private Subject                   subject;
    private Session                   session;
    private Date                      sessionDate;
    private Date                      confirmedDate;
    private VariableAssignmentManager _manager;

    private boolean         unsetDate                         = false;
    private boolean         isAutoArchiving                   = false;
    private boolean         warnOnDupeSessionLabels           = true;
    private boolean         allowOverwriteOnDupeSessionLabels = false;
    private boolean         allowAppendOnDupeSessionLabels    = true;
    private SessionVariable tracer                            = null;
}
