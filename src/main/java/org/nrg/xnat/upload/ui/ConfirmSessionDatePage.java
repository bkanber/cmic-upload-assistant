/*
 * upload-assistant: org.nrg.xnat.upload.ui.ConfirmSessionDatePage
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import com.toedter.calendar.JDateChooser;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;
import org.nrg.framework.exceptions.CanceledOperationException;
import org.nrg.framework.exceptions.NotFoundException;
import org.nrg.framework.ui.TraceableGridBagPanel;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.data.Subject;
import org.nrg.xnat.upload.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;
import static org.nrg.xnat.upload.ui.Constants.NO_DATE;

public final class ConfirmSessionDatePage extends WizardPage implements PropertyChangeListener {

    public static String getDescription() {
        return Messages.getPageTitle(ConfirmSessionDatePage.class);
    }

    public ConfirmSessionDatePage() {
        setLongDescription(Messages.getPageDescription(ConfirmSessionDatePage.class));

        _dateChooser = new JDateChooser(null, UIUtils.DEFAULT_DATE_FORMAT, JDateChooserTextFieldDataEditor.getInstance());
        _dateChooser.addPropertyChangeListener(this);

        _timeSpinner = new JSpinner(new SpinnerDateModel(Utils.trimDate(new Date()), null, null, Calendar.MINUTE));
        _timeSpinner.setEditor(new JSpinner.DateEditor(_timeSpinner, "HH:mm"));

        _noDate = new JCheckBox(Messages.getMessage("confirmsessiondatapage.no.date.description"), false);

        _visitList.setName("crVisit");
        _visitList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _visitList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int selectedIndex = ((JList) e.getSource()).getSelectedIndex();
                if (selectedIndex > -1 && selectedIndex < _visitListModel.getSize()) {
                    final VisitOccurrenceI selectedVisit = _visitListModel.get(selectedIndex);
                    logger.debug("selected visit " + selectedVisit.getName());

                    getSettings().setSelectedVisit(selectedVisit);

                    refreshVisitModalityList(selectedVisit);

                    userInputReceived((Component) e.getSource(), e);
                } else {
                    getSettings().clearSelectedVisit();
                    userInputReceived((Component) e.getSource(), e);
                }
            }
        });
        _visitList.setSelectionModel(new DefaultListSelectionModel() {
            @Override
            public boolean isSelectedIndex(int selectedIndex) {
                if (selectedIndex > -1 && selectedIndex < _visitListModel.getSize()) {
                    VisitOccurrenceI selectedVisit = _visitListModel.get(selectedIndex);

                    return !StringUtils.equals(selectedVisit.getStatus(), "Complete") && super.isSelectedIndex(selectedIndex);
                }
                return super.isSelectedIndex(selectedIndex);
            }
        });

        _modalityList.setName("crVisit-modality");
        _modalityList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _modalityList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                try {
                    int selectedIndex = ((JList) e.getSource()).getSelectedIndex();
                    if (selectedIndex > -1 && selectedIndex < _modalityListModel.getSize()) {
                        VisitModalityI selectedVisit = _modalityListModel.get(selectedIndex);

                        if (selectedVisit == null || (!selectedVisit.isEnabled())) {
                            getSettings().clearSelectedVisitModality();
                            getSettings().setExpectedModality(null);
                        } else {
                            logger.debug("selected modality " + selectedVisit.getLabel());
                            getSettings().setSelectedVisitModality(selectedVisit);
                            getSettings().setExpectedModality(selectedVisit.getModality());
                        }
                    } else {
                        getSettings().clearSelectedVisitModality();
                        getSettings().setExpectedModality(null);
                    }
                } catch (Exception e1) {
                    logger.error("", e1);
                }

                userInputReceived((Component) e.getSource(), e);
            }
        });
        _modalityList.setSelectionModel(new DefaultListSelectionModel() {
            @Override
            public boolean isSelectedIndex(int selectedIndex) {
                if (selectedIndex > -1 && selectedIndex < _modalityListModel.getSize()) {
                    VisitModalityI selectedVisit = _modalityListModel.get(selectedIndex);

                    return selectedVisit.isEnabled() && super.isSelectedIndex(selectedIndex);
                }
                return super.isSelectedIndex(selectedIndex);
            }
        });
    }

    /**
     * Indicates whether the user can click the Next button.
     *
     * @param stepName The name of the current step.
     * @param settings Any settings for the current step.
     * @param wizard   The current wizard.
     *
     * @return Returns <b>WizardPanelNavResult.REMAIN_ON_PAGE</b> if the user hasn't entered a confirmation date,
     * <b>WizardPanelNavResult.PROCEED</b> otherwise.
     */
    @Override
    public WizardPanelNavResult allowNext(final String stepName, final Map settings, final Wizard wizard) {
        getSettings().setCurrentSessionDate(_noDate.isSelected() ? NO_DATE : getDate());

        if (_visitTemplate != null) {
            getSettings().setSelectedVisit(_visitList.getSelectedValue());
            getSettings().setSelectedVisitModality(_modalityList.getSelectedValue());
        }

        return WizardPanelNavResult.PROCEED;
    }

    /*
     * (non-Javadoc)
     * @see org.netbeans.spi.wizard.WizardPage#renderingPage()
     */
    @Override
    protected void renderingPage() {
        logger.trace("rendering");

        setBusy(true);

        _project = getCurrentXnatServer().getCurrentProject();
        _subject = getSettings().getCurrentSubject();

        if (_project != null) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            try {
                while ((_visitTemplate = retrieveProjectVisitTemplate(_project)) == null) {
                    logger.info("Trying to retrieve the visit template for project {} failed, retrying at user request.", _project.toString());
                }
            } catch (CanceledOperationException e) {
                logger.error("The operation to retrieve the por");
            } catch (NotFoundException ignored) {
                logger.debug("Didn't find a visit template for project {}, continuing.", _project.toString());
            }
            setCursor(Cursor.getDefaultCursor());
        }

        final JPanel container = new TraceableGridBagPanel();
        add(container);

        try {
            _requireDate = _project.getRequireDateSetting();
        } catch (Exception exception) {
            logger.warn("Error when retrieving project require date setting", exception);
        }

        final Insets standardInsets = new Insets(5, 5, 5, 5);

        container.add(new JLabel(Messages.getMessage("confirmsessiondatapage.session.date.label")), new GridBagConstraints() {{
            gridx = 0;
            gridy = 0;
            ipadx = ipady = 5;
            anchor = GridBagConstraints.FIRST_LINE_START;
            insets = standardInsets;
        }});

        final JPanel dateOptions = new JPanel();
        dateOptions.setLayout(new FlowLayout(FlowLayout.LEADING));
        dateOptions.add(_dateChooser);

        if (_requireDate) {
            //require time as well
            dateOptions.add(_timeSpinner);
        }

        container.add(dateOptions, new GridBagConstraints() {{
            gridx = 1;
            gridy = 0;
            ipadx = ipady = 5;
            anchor = GridBagConstraints.FIRST_LINE_START;
            fill = GridBagConstraints.HORIZONTAL;
            insets = standardInsets;
        }});

        container.add(new JLabel(Messages.getFormattedMessage(_requireDate ? "confirmsessiondatapage.session.date.message" : "confirmsessiondatapage.session.date.message")), new GridBagConstraints() {{
            gridx = 1;
            gridy = 1;
            ipadx = ipady = 5;
            anchor = GridBagConstraints.FIRST_LINE_START;
            fill = GridBagConstraints.HORIZONTAL;
            insets = standardInsets;
        }});

        if (!_requireDate) {
            container.add(_noDate, new GridBagConstraints() {{
                gridx = 1;
                gridy = 2;
                ipadx = ipady = 5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                fill = GridBagConstraints.HORIZONTAL;
                insets = standardInsets;
            }});
        }

        if (_visitTemplate != null) {
            refreshVisitList();
            refreshVisitModalityList(null);

            container.add(new JSeparator(SwingConstants.HORIZONTAL), new GridBagConstraints() {{
                gridy = 2;
                ipadx = ipady = 3;
                anchor = GridBagConstraints.FIRST_LINE_START;
                gridwidth = 3;
                fill = GridBagConstraints.HORIZONTAL;
                insets = standardInsets;
            }});
            container.add(new JLabel(Messages.getMessage("confirmsessiondatapage.visit.label")), new GridBagConstraints() {{
                gridx = 0;
                gridy = 4;
                ipadx = ipady = 5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                insets = standardInsets;
            }});
            container.add(new JLabel(Messages.getMessage("confirmsessiondatapage.modality.label")), new GridBagConstraints() {{
                gridx = 1;
                gridy = 4;
                ipadx = ipady = 5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                insets = standardInsets;
            }});
            container.add(new JScrollPane(_visitList), new GridBagConstraints() {{
                gridx = 0;
                gridy = 5;
                ipadx = ipady = 5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                fill = GridBagConstraints.HORIZONTAL;
                insets = standardInsets;
            }});
            container.add(new JScrollPane(_modalityList), new GridBagConstraints() {{
                gridx = 1;
                gridy = 5;
                ipadx = ipady = 5;
                anchor = GridBagConstraints.FIRST_LINE_START;
                fill = GridBagConstraints.HORIZONTAL;
                insets = standardInsets;
            }});
        }

        setProblem(Messages.getMessage("confirmsessiondatapage.session.date.problem"));

        setBusy(false);
    }

    /**
     * Validates that either a date is specified in the date chooser or that the "no date" checkbox is checked. If there
     * is a visit template configured, this method also tests to ensure that a visit and modality are selected.
     *
     * @param component The component being validated.
     * @param event     The event that occurred.
     *
     * @return Returns null if the contents of the dialog are valid. Otherwise this returns a message to display.
     */
    @Override
    protected String validateContents(final Component component, final Object event) {
        if (_dateChooser.getDate() == null && !_noDate.isSelected()) {
            return Messages.getMessage("confirmsessiondatapage.session.date.problem");
        }
        if (_visitTemplate != null) {
            if (_visitList.isSelectionEmpty()) {
                return Messages.getMessage("confirmsessiondatapage.visit.problem");
            }
            if (_modalityList.isSelectionEmpty()) {
                return Messages.getMessage("confirmsessiondatapage.visit.modality.problem");
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void propertyChange(final PropertyChangeEvent event) {
        if (event.getPropertyName().startsWith("date")) {
            userInputReceived(_dateChooser, event);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void recycle() {
        removeAll();
        validate();
    }

    /**
     * This method attempts to retrieve the visit template for the specified project. It has four possible results:
     *
     * <ul>
     * <li>Returns the visit template for the project</li>
     * <li>
     * Throws {@link NotFoundException}, indicating that there is no visit template for the project. This is
     * an acceptable result for the call and can be ignored. This allows retries in a while loop, providing the
     * exception to break from the while loop.
     * </li>
     * <li>
     * Returns null. This is actually an exceptional state: it indicates an exception occurred trying to
     * retrieve the visit template, but the user elected to retry the operation.
     * </li>
     * <li>
     * Throws <b>CanceledOperationException</b>, which indicates that an exception occurred trying to retrieve
     * the visit template and the user elected to not retry the operation.
     * </li>
     * </ul>
     *
     * @param project The project to query for a visit template.
     *
     * @return The visit template if found or null if the operation failed and should be retried.
     *
     * @throws NotFoundException          Indicates that a visit template was not found for the project.
     * @throws CanceledOperationException Indicates that the operation failed and shouldn't be retried.
     */
    private VisitTemplateI retrieveProjectVisitTemplate(final Project project) throws NotFoundException, CanceledOperationException {
        try {
            final VisitTemplateI template = project.getVisitTemplate();
            if (template == null) {
                throw new NotFoundException("");
            }
            return template;
        } catch (InterruptedException | ExecutionException e) {
            final Throwable cause = e.getCause();
            if (e instanceof InterruptedException) {
                logger.error("Interrupted trying to retrieve the visit template for the project " + _project.toString(), e);
                promptForRetry(e);
            } else if (cause instanceof IOException || cause instanceof JSONException) {
                promptForRetry(cause);
            } else {
                logger.error("error getting visit list for " + _subject.getLabel() + " in project " + _project.toString(), cause);
                logger.info("will retry in 1000 ms");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignore) {
                    JOptionPane.showMessageDialog(this, Messages.getFormattedMessage("error.generic.message", ignore.getMessage()), Messages.getMessage("error.generic.title"), JOptionPane.ERROR_MESSAGE);
                }
            }
            return null;
        }
    }

    /**
     * Refreshes the list of visits in the current project, re-selecting the current selection after refresh completes.
     */
    private void refreshVisitList() {
        _visitListModel.removeAllElements();
        for (final VisitOccurrenceI occurrence : _visitTemplate.getVisits()) {
            occurrence.setSubject(_subject);
            _visitListModel.addElement(occurrence);
        }
    }

    private void refreshVisitModalityList(final VisitOccurrenceI visit) {
        getSettings().clearSelectedVisitModality();
        _modalityListModel.removeAllElements();
        if (visit != null) {
            for (final VisitModalityI modality : visit.getModalities()) {
                modality.setSubject(_subject);
                _modalityListModel.addElement(modality);
            }
        }
    }

    private Date getDate() {
        final Date baseDate = _dateChooser.getDate();
        if ((baseDate != null)) {
            if (_requireDate) {
                return Utils.trimDate(baseDate);
            } else {
                final Calendar date = Calendar.getInstance();
                date.setTime(Utils.trimDate(baseDate));

                final Calendar time = Calendar.getInstance();
                time.setTime((Date) _timeSpinner.getValue());

                final Calendar calendar = Calendar.getInstance();
                calendar.clear();
                calendar.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH));
                calendar.set(Calendar.MONTH, date.get(Calendar.MONTH));
                calendar.set(Calendar.YEAR, date.get(Calendar.YEAR));
                calendar.set(Calendar.HOUR, time.get(Calendar.HOUR));
                calendar.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
                return calendar.getTime();
            }
        }
        return null;
    }

    private void promptForRetry(final Throwable cause) throws CanceledOperationException {
        final boolean isIOException = cause instanceof IOException || cause instanceof InterruptedException;
        final String title = Messages.getMessage(isIOException ? "confirmsessiondatapage.visit.ioexception.title" : "confirmsessiondatapage.visit.jsonexception.title");
        final String messageKey = isIOException ? "confirmsessiondatapage.visit.ioexception.message" : "confirmsessiondatapage.visit.jsonexception.message";
        final Object[] options = {"Retry", "Cancel"};
        final int response = JOptionPane.showOptionDialog(ConfirmSessionDatePage.this,
                                                          Messages.getFormattedMessage(messageKey, getCurrentXnatServer().getAddress()),
                                                          title,
                                                          JOptionPane.YES_NO_OPTION,
                                                          JOptionPane.ERROR_MESSAGE,
                                                          null,
                                                          options,
                                                          options[0]);
        switch (response) {
            case JOptionPane.YES_OPTION:
                logger.error("error getting visit list; retrying at user request", cause);

            default:
                logger.error("error getting visit list; cancelling at user request", cause);
                throw new CanceledOperationException(cause);
        }
    }

    private static final Logger logger = LoggerFactory.getLogger(ConfirmSessionDatePage.class);

    private final DefaultListModel<VisitOccurrenceI> _visitListModel    = new DefaultListModel<>();
    private final JList<VisitOccurrenceI>            _visitList         = new JList<>(_visitListModel);
    private final DefaultListModel<VisitModalityI>   _modalityListModel = new DefaultListModel<>();
    private final JList<VisitModalityI>              _modalityList      = new JList<>(_modalityListModel);

    private final JDateChooser _dateChooser;
    private final JSpinner     _timeSpinner;
    private final JCheckBox    _noDate;

    private Project        _project       = null;
    private Subject        _subject       = null;
    private VisitTemplateI _visitTemplate = null;
    private boolean        _requireDate   = false;
}
