package org.nrg.xnat.upload.ui;

import org.apache.commons.lang3.StringUtils;
import org.netbeans.spi.wizard.WizardPage;
import org.nrg.xnat.upload.data.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DoubleClickListener extends MouseAdapter {
    public DoubleClickListener(final WizardPage page) {
        _page = page;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final MouseEvent event) {
        final Object source = event.getSource();
        final JList  list   = (JList) source;
        if (event.getClickCount() == 2 && event.getButton() == MouseEvent.BUTTON1) {
            // Double-click detected
            final Object selected = list.getSelectedValue();
            final String value = selected.toString();
            if (selected instanceof String) {
                _log.info("The currently selected project is {}", value);
            } else if (selected instanceof Subject) {
                _log.info("The currently selected subject is {}", value);
            } else {
                _log.info("I don't know what it is, but the currently selected thing is {}", value);
            }
            final JButton button = _page.getRootPane().getDefaultButton();
            if (StringUtils.equals("next", button.getName())) {
                button.doClick();
            }
        }
    }

    private static final Logger _log = LoggerFactory.getLogger(DoubleClickListener.class);
    private final WizardPage _page;
}
