/*
 * upload-assistant: org.nrg.xnat.upload.ui.NewSubjectDialog
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import com.google.common.base.Strings;
import org.nrg.framework.ui.TraceableGridBagPanel;
import org.nrg.xnat.Labels;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.data.Subject;
import org.nrg.xnat.upload.data.SubjectInformation;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.concurrent.Callable;

public class NewSubjectDialog extends AssistantDialog {
    private static final long serialVersionUID = 1L;
    private static final String TITLE = "Create new subject";    // TODO: localize
    private static final String CREATE_LABEL = "Create";    // TODO: localize
    private static final String CANCEL_LABEL = "Cancel";    // TODO: localize
    private static final String ID_LABEL = "Subject label:";    // TODO: localize
    private static final String EMPTY_MESSAGE = " ";
    private static final String EMPTY_LABEL_MESSAGE = "Label cannot be empty.";
    private static final String INVALID_LABEL_MESSAGE = "Label may contain only letters, digits, hyphen (-) and underscore (_).";
    private static final String EXISTING_SUBJECT_MESSAGE = "Subject %s already exists.";

    private final Logger _log = LoggerFactory.getLogger(NewSubjectDialog.class);
    private final SelectSubjectPage page;
    private final Project           project;
    private JPanel contents = null;


    public NewSubjectDialog(final SelectSubjectPage page, final Project project) {
        super(UIUtils.findParentFrame(page), TITLE, true);
        setLocationRelativeTo(getOwner());
        this.page = page;
        this.project = project;
        setContentPane(getContents());
        pack();
    }

    private void doCreateSubject(final String label) {
        final Callable<Subject> doCreate = new Callable<Subject>() {
            public Subject call() throws SubjectInformation.UploadSubjectException {
                final SubjectInformation subjectInfo = new SubjectInformation(project);
                subjectInfo.setLabel(label);
                try {
                    final Subject subject = subjectInfo.uploadTo();
                    project.addSubject(subject);
                    page.refreshSubjectList(subject);
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            NewSubjectDialog.this.dispose();
                        }
                    });
                    return subject;
                } catch (SubjectInformation.UploadSubjectException e) {
                    _log.error("Unable to create subject " + label, e);
                    JOptionPane.showMessageDialog(NewSubjectDialog.this,
                                                  Messages.getFormattedMessage("newsubjectdialog.error.message", label, e.getMessage()),
                                                  Messages.getMessage("newsubjectdialog.error.title"),
                                                  JOptionPane.ERROR_MESSAGE);
                    throw e;
                }
            }
        };
        project.submit(doCreate);
    }

    private JPanel getContents() {
        if (null == contents) {
            contents = new TraceableGridBagPanel();

            final JLabel idLabel = makeLabel(ID_LABEL);
            final JLabel idMessage = makeMessage(" ");
            final JTextField idTF = makeTextField();

            final JButton cancelButton = new JButton(CANCEL_LABEL);
            final JButton createButton = new JButton(CREATE_LABEL);

            idMessage.setForeground(Color.RED);

            idTF.getDocument().addDocumentListener(new DocumentListener() {
                private void handle(final DocumentEvent event) {
                    if (_log.isDebugEnabled()) {
                        _log.debug("Handling event " + event.getType().toString());
                    }
                    final String text = idTF.getText();
                    if (Strings.isNullOrEmpty(text)) {
                        idMessage.setText(EMPTY_LABEL_MESSAGE);
                        createButton.setEnabled(false);
                    } else if (!Labels.isValidLabel(text)) {
                        idMessage.setText(INVALID_LABEL_MESSAGE);
                        createButton.setEnabled(false);
                    } else {
                        try {
                            if (project.hasSubject(text)) {
                                idMessage.setText(String.format(EXISTING_SUBJECT_MESSAGE, text));
                                createButton.setEnabled(false);
                            } else {
                                idMessage.setText(EMPTY_MESSAGE);
                                createButton.setEnabled(true);
                            }
                        } catch (Throwable e) {
                            _log.error("unable to check for subject " + text + " in " + project, e);
                            idMessage.setText(EMPTY_MESSAGE);
                            createButton.setEnabled(true);
                        }
                    }
                }

                public void removeUpdate(DocumentEvent e) {
                    handle(e);
                }

                public void insertUpdate(DocumentEvent e) {
                    handle(e);
                }

                public void changedUpdate(DocumentEvent e) {
                    handle(e);
                }
            });

            // Start with empty label text, so create button must be disabled
            idMessage.setText(EMPTY_LABEL_MESSAGE);
            createButton.setEnabled(false);

            cancelButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent ev) {
                    NewSubjectDialog.this.dispose();
                }
            });

            createButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(final MouseEvent ev) {
                    String label = idTF.getText();
                    if (Labels.isValidLabel(label)) {
                        createButton.setEnabled(false);
                        cancelButton.setEnabled(false);
                        doCreateSubject(label);
                    } else {
                        // This should actually already be true, but some impls, e.g. Mac OS X, allow you to click button anyways.
                        idMessage.setText(INVALID_LABEL_MESSAGE);
                        createButton.setEnabled(false);
                    }
                }
            });

            contents.add(idLabel, makeLabelConstraints(0));
            contents.add(idTF, makeValueConstraints(0));
            contents.add(idMessage, makeMessageConstraints(1));
            contents.add(cancelButton, makeButtonConstraints(1, 2));
            contents.add(createButton, makeButtonConstraints(2, 2));
        }
        return contents;
    }

}
