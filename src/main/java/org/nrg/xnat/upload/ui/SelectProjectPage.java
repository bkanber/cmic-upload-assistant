/*
 * upload-assistant: org.nrg.xnat.upload.ui.SelectProjectPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;

public final class SelectProjectPage extends WizardPage {
    public static String getDescription() {
        return Messages.getPageTitle(SelectProjectPage.class);
    }

    public SelectProjectPage(final Dimension dimensions) throws IOException, JSONException {
        super();

        // We could just load the JList with Project objects.  Instead, we load it with
        // labels and create the Projects dynamically upon selection, because the Project
        // constructor spins off threads to retrieve the contained subjects and sessions.
        // This is smart if we're creating one Project at a time, but troublesome when
        // creating lots of Projects.
        logger.trace("initializing SelectProjectPage; querying XNAT for project labels");
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        _projects = new JList<>();
        _projects.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _projects.addMouseListener(new DoubleClickListener(this));

        _dimensions = dimensions;

        setOpaque(true);
        setLongDescription(Messages.getPageDescription(SelectProjectPage.class));
        setCursor(Cursor.getDefaultCursor());
    }

    /**
     * Removes all existing controls to trigger re-rendering of the page.
     */
    public void recycle() {
        removeAll();
    }

    /**
     * {@inheritDoc}
     */
    public void renderingPage() {
        logger.trace("rendering SelectProjectPage");
        try {
            final List<String> projects = getCurrentXnatServer().getProjects();
            if (projects.size() > 0) {
                Collections.sort(projects);
                _projects.setListData(new Vector<>(projects));
                _projects.setEnabled(true);
                logger.trace("SelectProjectPage ready with projects {}", Arrays.toString(_projects.getComponents()));
            } else {
                _projects.setListData(new Vector<>(Collections.singletonList(Messages.getMessage("selectprojectpage.noprojects.message", getCurrentXnatServer().getAddress()))));
                _projects.setEnabled(false);
                logger.trace("SelectProjectPage has no projects for server ", getCurrentXnatServer().getAddress());
            }
        } catch (IOException e) {
            logger.error("An error occurred trying to retrieve the list of projects from the current server at " + getCurrentXnatServer().getAddress(), e);
        }
        final JScrollPane pane = new JScrollPane(_projects);
        if (null != _dimensions) {
            pane.setPreferredSize(_dimensions);
        }
        add(pane);
    }

    /**
     * Indicates whether the state of the page is valid.
     *
     * @param component The component to be validated.
     * @param object    The object, usually an event.
     *
     * @return Returns null if the page state is valid, otherwise returns a message indicating what needs to be done.
     */
    protected String validateContents(final Component component, final Object object) {
        if (object != null && ((ListSelectionEvent) object).getValueIsAdjusting()) {
            return _validation;
        }
        return _validation = _projects.getSelectedIndex() < 0 ? "" : null;
    }

    /**
     * This always allows the user to go back, but removes the project object set in the settings object.
     *
     * @param stepName The name of the current step.
     * @param settings Any settings for the current step.
     * @param wizard   The current wizard.
     *
     * @return Returns <b>WizardPanelNavResult.PROCEED</b> in all cases.
     */
    public WizardPanelNavResult allowBack(String stepName, Map settings, Wizard wizard) {
        getCurrentXnatServer().setCurrentProject(null);
        return WizardPanelNavResult.PROCEED;
    }

    /**
     * Indicates whether the user can proceed to the next page. For this page, the current directory in the file chooser
     * is set as the current directory in preferences to be reused for future operations. There are no checks that block
     * the user's ability to proceed.
     *
     * @param stepName The name of the current step.
     * @param settings Any settings for the current step.
     * @param wizard   The current wizard.
     *
     * @return Returns <b>WizardPanelNavResult.PROCEED</b> in all cases.
     */
    @Override
    public WizardPanelNavResult allowNext(final String stepName, final Map settings, final Wizard wizard) {
        final String projectId = _projects.getSelectedValue();
        if (StringUtils.isNotBlank(projectId)) {
            getCurrentXnatServer().setCurrentProject(projectId);
            return WizardPanelNavResult.PROCEED;
        }
        return WizardPanelNavResult.REMAIN_ON_PAGE;
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        final Project project = getCurrentXnatServer().getCurrentProject();
        return project != null ? project.toString() : "";
    }

    private final Logger logger = LoggerFactory.getLogger(SelectProjectPage.class);

    private final JList<String> _projects;
    private final Dimension     _dimensions;

    private String _validation = "";
}
