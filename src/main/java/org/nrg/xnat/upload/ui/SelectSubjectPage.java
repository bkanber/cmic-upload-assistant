/*
 * upload-assistant: org.nrg.xnat.upload.ui.SelectSubjectPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.json.JSONException;
import org.netbeans.spi.wizard.WizardPage;
import org.nrg.xnat.upload.application.UploadAssistant;
import org.nrg.xnat.upload.data.Project;
import org.nrg.xnat.upload.data.Subject;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import static org.nrg.xnat.upload.application.UploadAssistant.getCurrentXnatServer;
import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;

public class SelectSubjectPage extends WizardPage {

    private static final Logger logger = LoggerFactory.getLogger(SelectSubjectPage.class);
    private final JList<Subject> list;
    private final DefaultListModel<Subject> listModel = new DefaultListModel<>();
    private final Dimension dimension;
    private Project _project;

    public static String getDescription() {
        return Messages.getPageTitle(SelectSubjectPage.class);
    }

    public SelectSubjectPage(final Dimension dimension) {
        this.dimension = dimension;
        list = new JList<>(listModel);
        list.setName("subject");
        list.addMouseListener(new DoubleClickListener(this));
        setLongDescription(Messages.getPageDescription(SelectSubjectPage.class));
    }

    /*
      * (non-Javadoc)
      * @see org.netbeans.spi.wizard.WizardPage#recycle()
      */
    protected void recycle() {
        removeAll();
        listModel.clear();
        validate();
    }

    /*
      * (non-Javadoc)
      * @see org.netbeans.spi.wizard.WizardPage#renderingPage()
      */
    protected void renderingPage() {
        setBusy(true);
        _project = getCurrentXnatServer().getCurrentProject();
        final JScrollPane subjectList = new JScrollPane(list);
        if (null != dimension) {
            subjectList.setPreferredSize(dimension);
        }
        add(subjectList);
        refreshSubjectList();
        if (_project != null && _project.allowCreateSubject()) {
            final JButton newSubject = new JButton("Create new subject");

            final JDialog newSubjectDialog = new NewSubjectDialog(this, _project);
            newSubject.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    newSubjectDialog.setVisible(true);
                }
            });
            newSubject.setEnabled(true);
            add(newSubject);
        }
        setBusy(false);
    }

    /*
      * (non-Javadoc)
      * @see org.netbeans.spi.wizard.WizardPage#validateContents(java.awt.Component, java.lang.Object)
      */
    protected String validateContents(final Component component, final Object o) {
        if (o instanceof ListSelectionEvent) {
            getSettings().setCurrentSubject(list.isSelectionEmpty() ? null : list.getSelectedValue());
        }
        return getSettings().getCurrentSubject() == null ? "Select a subject to proceed" : null;
    }

    /**
     * Refreshes the list of subjects in the current project.
     *
     * @param selection item in the subjects list to be selected after refresh
     */
    void refreshSubjectList(final Object selection) {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        getSettings().setCurrentSubject(null);
        final Callable<Object> doRefresh = new Callable<Object>() {
            public Object call() throws IOException, JSONException {
                for (; ; ) {
                    listModel.removeAllElements();
                    try {
                        final SortedSet<Subject> subjects = new TreeSet<>(new Subject.SubjectComparator());
                        final Collection<Subject> found = _project.getSubjects();
                        logger.info("Found {} subjects for the project {}", found.size(), _project.toString());
                        subjects.addAll(found);

                        for (final Subject subject : subjects) {
                            logger.info("Adding subject [{}] {}", subject.getId(), subject.getLabel());
                            if (!listModel.contains(subject)) {
                                listModel.addElement(subject);
                            }
                        }
                        break;
                    } catch (InterruptedException retry) {
                        logger.info("subject retrieval interrupted, retrying", retry);
                    } catch (ExecutionException e) {
                        final Throwable cause = e.getCause();
                        if (cause instanceof IOException) {
                            final Object[] options = {"Retry", "Cancel"};
                            final int n = JOptionPane.showOptionDialog(SelectSubjectPage.this,
                                                                       Messages.getFormattedMessage("selectsubjectpage.networkerror.message", getCurrentXnatServer().getAddress(), cause.getMessage()),
                                                                       Messages.getMessage("selectsubjectpage.networkerror.title"),
                                                                       JOptionPane.YES_NO_OPTION,
                                                                       JOptionPane.ERROR_MESSAGE,
                                                                       null,
                                                                       options,
                                                                       options[0]);
                            if (JOptionPane.NO_OPTION == n) {
                                throw new IOException(cause);
                            } else {
                                logger.error("error getting subject list; retrying at user request", cause);
                            }
                        } else if (cause instanceof JSONException) {
                            final Object[] options = {"Retry", "Cancel"};
                            final int n = JOptionPane.showOptionDialog(SelectSubjectPage.this,
                                                                       Messages.getFormattedMessage("selectsubjectpage.servererror.message", getCurrentXnatServer().getAddress()),
                                                                       Messages.getMessage("selectsubjectpage.servererror.title"),
                                                                       JOptionPane.YES_NO_OPTION,
                                                                       JOptionPane.ERROR_MESSAGE,
                                                                       null,
                                                                       options,
                                                                       options[0]);
                            if (JOptionPane.NO_OPTION == n) {
                                throw new IOException(cause);
                            } else {
                                logger.error("error getting subject list; retrying at user request", cause);
                            }
                        } else {
                            logger.error("error getting subject list for " + _project, cause);
                            logger.info("will retry in 1000 ms");
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException ignore) {
                                JOptionPane.showMessageDialog(UploadAssistant.getInstance(),
                                                              Messages.getFormattedMessage("error.generic.message", ignore.getMessage()),
                                                              Messages.getMessage("error.generic.title"),
                                                              JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        selectSubject(selection);
                    }
                });
                return selection;
            }
        };
        _project.submit(doRefresh);
        setCursor(Cursor.getDefaultCursor());
    }

    /**
     * Refreshes the list of subjects in the current project, re-selecting
     * the current selection after refresh is complete.
     */
    void refreshSubjectList() {
        refreshSubjectList(getSettings().getCurrentSubject());
    }

    private void selectSubject(Object selection) {
        if (listModel.contains(selection)) {
            list.setSelectedValue(selection, true);
        }
    }
}
