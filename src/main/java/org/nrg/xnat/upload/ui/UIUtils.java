/*
 * upload-assistant: org.nrg.xnat.upload.ui.UIUtils
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import com.google.common.base.Predicates;
import com.google.common.base.Strings;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.nrg.xnat.upload.data.SessionVariable;
import org.nrg.xnat.upload.util.Messages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;

import static org.nrg.xnat.upload.application.UploadAssistant.getSettings;

public class UIUtils {
    private UIUtils() {
    }

    public static final String DEFAULT_DATE_FORMAT = "d MMM yyyy";
    public static final String VERIFY_XNAT_PATH    = "/data/auth";

    public static Frame findParentFrame(final Component component) {
        for (Component c = component; null != c; c = c.getParent()) {
            if (c instanceof Frame) {
                return (Frame) c;
            }
        }
        return null;
    }

    /**
     * Detects whether the address has any protocol specified.
     *
     * @param address The address to test.
     *
     * @return Returns true if the address starts with "http://" or "https://". false otherwise.
     */
    public static boolean hasProtocol(final String address) {
        return address.matches("^http[s]?://.*$");
    }

    /**
     * Returns a URL trimmed of trailing slashes.
     *
     * @param url The URL to create.
     *
     * @return A URL with no trailing slash.
     */
    public static URL getCleanUrl(final String url) {
        try {
            return new URL(url.endsWith("/") ? StringUtils.removeEnd(url, "/") : url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);    // can't imagine how this would happen
        }
    }

    /**
     * Returns a URL trimmed of trailing slashes.
     *
     * @param url The URL to create.
     *
     * @return A URL with no trailing slash.
     */
    public static URL getCleanUrl(final URL url) {
        if (url.toString().endsWith("/")) {
            return getCleanUrl(url.toString());
        }
        return url;
    }

    /**
     * Returns a clean URL with either the protocol already specified in the address or, if no protocol is specified,
     * the HTTPS protocol.
     *
     * @param address The address from which the URL should be created.
     *
     * @return A URL for the submitted address.
     *
     * @throws MalformedURLException If the address doesn't form a valid URL.
     */
    public static URL getUrlWithProtocol(final String address) throws MalformedURLException {
        final boolean isProtocolSpecified = hasProtocol(address);
        return isProtocolSpecified ? getCleanUrl(address) : getCleanUrl("https://" + address);
    }

    /**
     * Creates a clean URL with the protocol determined through the {@link #getUrlWithProtocol(String)} method and the
     * {@link #VERIFY_XNAT_PATH} REST path appended.
     *
     * @param address The address from which the URL should be created.
     *
     * @return The cleaned REST URL for the submitted address.
     *
     * @throws MalformedURLException If the address doesn't form a valid URL.
     */
    public static URL getVerifyXnatUrl(final String address) throws MalformedURLException {
        return new URL(getUrlWithProtocol(address).toString() + VERIFY_XNAT_PATH);
    }

    /**
     * Handles application errors in a consistent way.
     *
     * @param component The component from which the error was dispatched.
     * @param throwable A <b>Throwable</b> error object.
     */
    public static void handleApplicationError(Component component, Throwable throwable) {
        String message = throwable.getMessage();
        String formattedMessage = StringUtils.isBlank(message)
                                  ? Messages.getFormattedMessage("error.exception.info.withoutmessage", throwable.getClass().getName(), formatStackTrace(throwable.getStackTrace()))
                                  : Messages.getFormattedMessage("error.exception.info.withmessage", throwable.getClass().getName(), message + formatStackTrace(throwable.getStackTrace()));
        handleApplicationError(component, formattedMessage);
    }

    /**
     * Handles user errors in a consistent way. This is similar to the {@link #handleApplicationError(Component, String, String, int)}
     * method, but doesn't log the error since there's no need to debug it.
     *
     * @param component   The component from which the error was dispatched.
     * @param message     A message indicating the error.
     * @param title       The title for the error message.
     * @param messageType The type of error display.
     */
    public static void handleUserError(final Component component, final String message, final String title, final int messageType) {
        if (component != null) {
            JOptionPane.showMessageDialog(component, message, StringUtils.isNotBlank(title) ? title : Messages.getMessage("error.generic.title"), messageType);
        }
    }

    /**
     * Handles application errors in a consistent way.
     *
     * @param component The component from which the error was dispatched.
     * @param message   A message indicating the error.
     */
    public static void handleApplicationError(Component component, String message) {
        handleApplicationError(component, message, Messages.getMessage("error.generic.title"), JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Handles application errors in a consistent way.
     *
     * @param component   The component from which the error was dispatched.
     * @param message     A message indicating the error.
     * @param title       The title for the error message.
     * @param messageType The type of error display.
     */
    public static void handleApplicationError(Component component, String message, String title, int messageType) {
        Logger log;
        if (component != null) {
            if (StringUtils.isBlank(title)) {
                title = Messages.getMessage("error.generic.title");
            }
            JOptionPane.showMessageDialog(component, message, title, messageType);
            log = LoggerFactory.getLogger(component.getClass());
        } else {
            log = _log;
        }
        log.error(message);
    }

    /**
     * Displays a modal input box with the indicated message, title, and message type, which should be taken from the
     * <b>{@link JOptionPane}</b> class. The default value is set to the indicated value. Returns the input user value.
     *
     * @param component    The component from which the input request was dispatched.
     * @param defaultValue The default value to be populated.
     */
    public static void handleUrlClick(Component component, String defaultValue) {
        JOptionPane.showInputDialog(component,
                                    Messages.getFormattedMessage(Messages.ERROR_MSG_NOURLSUPPORT),
                                    Messages.getMessage(Messages.ERROR_TITLE_NOURLSUPPORT),
                                    JOptionPane.ERROR_MESSAGE, null, null, defaultValue);
    }

    public static String formatStackTrace(StackTraceElement[] elements) {
        if (elements == null || elements.length == 0) {
            return "";
        }
        StringBuilder buffer = new StringBuilder();
        for (StackTraceElement element : elements) {
            buffer.append(element.toString()).append("\n");
        }
        return buffer.toString();
    }

    public static boolean getConfirmSessionDatePage() {
        return Boolean.parseBoolean(System.getProperty("verification.date.display", "true"));
    }

    private static final Logger _log = LoggerFactory.getLogger(UIUtils.class);

    public static JSONObject buildCommitEntity() {
        final JSONObject entity = new JSONObject();
        final Map<String, SessionVariable> variables = getSettings().getSessionVariables();
        if (null == variables) {
            _log.error("session variables not assigned");
        } else {
            for (final SessionVariable variable : variables.values()) {
                final String path = variable.getExportField();
                if (!Strings.isNullOrEmpty(path)) {
                    try {
                        entity.put(path, variable.getValue());
                    } catch (JSONException exception) {
                        _log.error("unable to assign session variable " + path, exception);
                    }
                }
            }
        }
        _log.trace("Built commit entity: {}", entity);
        return entity;
    }

    public static URL validateXnatServerUrl(final URL url) throws MalformedURLException {
        if (StringUtils.isBlank(url.getHost())) {
            throw new MalformedURLException("host");
        }
        if (StringUtils.isNotBlank(url.getQuery())) {
            throw new MalformedURLException("query");
        }
        if (StringUtils.isNotBlank(url.getRef())) {
            throw new MalformedURLException("ref");
        }
        if (getXnatServerPathElements(url) > 1) {
            throw new MalformedURLException("path");
        }
        return getCleanUrl(url);
    }

    public static int getXnatServerPathElements(final URL url) {
        return Collections2.filter(Arrays.asList(url.getPath().split("/")), Predicates.not(Predicates.equalTo(""))).size();
    }

    public static String formatXnatServerName(final String address, final String username) {
        try {
            // If the address has a protocol specified, we'll use that, otherwise we'll use http:// since it doesn't
            // really matter for our purposes here.
            final URL url = hasProtocol(address) ? new URL(address) : new URL("http://" + address);
            return formatXnatServerName(validateXnatServerUrl(url), username);
        } catch (MalformedURLException e) {
            _log.error("The URL " + address + " is not valid, please try again.");
            return null;
        }
    }

    public static String formatXnatServerName(final URL url, final String username) {
        return StringUtils.defaultIfBlank(username, "<no user>") +
               " @ " + url.getHost() +
               (StringUtils.isNotBlank(url.getPath()) ? StringUtils.replace("/" + url.getPath(), "//", "/") : "");
    }
}
