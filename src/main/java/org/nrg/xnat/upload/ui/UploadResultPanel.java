/*
 * upload-assistant: org.nrg.xnat.upload.ui.UploadResultPanel
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.nrg.xnat.upload.util.Messages;

import javax.swing.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public final class UploadResultPanel extends JPanel {
    public UploadResultPanel(final String label, final URL url) {
        final boolean isArchived = url.getPath().split("/")[3].equalsIgnoreCase("archive");
        final JLabel link = getLinkForResource(label, url);

        final Box box = new Box(BoxLayout.Y_AXIS);
        add(box);
        box.add(new JLabel(Messages.getFormattedMessage(Messages.UPLOADRESULTPANEL_SUCCESS, Messages.getMessage(isArchived ? Messages.VOCABULARY_ARCHIVE : Messages.VOCABULARY_PREARCHIVE))));
        box.add(Box.createVerticalStrut(Constants.SPACING));
        box.add(link);
    }

    public JLabel getLinkForResource(final String label, final URL url) {
        final JLabel link = new JLabel(Messages.getFormattedMessage(Messages.UPLOADRESULTPANEL_DEST_LINK, url, label));
        link.setCursor(Constants.LINK_CURSOR);
        try {
            final URI uri = new URI(url.toString()); //url.toURI();
            link.addMouseListener(new UriClickListener(uri));
        } catch (URISyntaxException e) {
            UIUtils.handleApplicationError(this, Messages.getFormattedMessage("uploadresultpanel.error.message", e.getMessage()), Messages.getMessage("uploadresultpanel.error.title"), JOptionPane.ERROR_MESSAGE);
        }
        return link;
    }
}
