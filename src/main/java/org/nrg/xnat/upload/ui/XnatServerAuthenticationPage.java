/*
 * upload-assistant: org.nrg.xnat.upload.ui.SelectSessionPage
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.ui;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.netbeans.spi.wizard.Wizard;
import org.netbeans.spi.wizard.WizardPage;
import org.netbeans.spi.wizard.WizardPanelNavResult;
import org.nrg.xnat.upload.application.UploadAssistantSettings;
import org.nrg.xnat.upload.net.XnatConnectionException;
import org.nrg.xnat.upload.net.XnatServer;
import org.nrg.xnat.upload.util.Messages;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;

import static org.nrg.xnat.upload.application.UploadAssistant.getRestServer;

public final class XnatServerAuthenticationPage extends WizardPage implements PropertyChangeListener {
    public static String getDescription() {
        return Messages.getMessage("xnatserverauthenticationpage.step");
    }

    public XnatServerAuthenticationPage(final UploadAssistantSettings settings) throws Exception {
        setLayout(_layout = new CardLayout());
        setLongDescription(Messages.getPageDescription(this.getClass()));

        _settings = settings;

        add(_authenticationPanel = new XnatServerAuthenticationPanel(this, _settings, false), AUTHENTICATION_PANEL_NAME);
        add(_configurationPanel = new XnatServerConfigurationPanel(this, _settings, false), CONFIGURATION_PANEL_NAME);
    }

    public void refresh() {
        _authenticationPanel.adjustPanelState();
    }

    /**
     * Indicates whether the user can proceed to the next page. For this page, the values in the various controls will
     * be used to authenticate against the target XNAT server. If authentication fails, the user is informed in a
     * message dialog and not allowed to proceed. If authentication is successful, the authentication information is
     * cached and set as the {@link UploadAssistantSettings#setCurrentXnatServer(XnatServer) current server}. The user
     * is allowed to proceed to the next page.
     *
     * @param stepName The name of the current step.
     * @param settings Any settings for the current step.
     * @param wizard   The current wizard.
     *
     * @return Returns <b>WizardPanelNavResult.REMAIN_ON_PAGE</b> if the user hasn't entered valid URL and
     * authentication credentials, <b>WizardPanelNavResult.PROCEED</b> otherwise.
     */
    @Override
    public WizardPanelNavResult allowNext(final String stepName, final Map settings, final Wizard wizard) {
        final XnatServer server   = _authenticationPanel.getSelectedXnatServer();
        final char[]     password = _authenticationPanel.getPassword();
        if (ArrayUtils.isNotEmpty(password)) {
            server.setPassword(password);
        }

        try {
            getRestServer().verifyServer(server);
            _settings.setCurrentXnatServer(server);
            return WizardPanelNavResult.PROCEED;
        } catch (XnatConnectionException e) {
            final String address = server.getAddress();
            final String message, title;
            switch (e.getFailure()) {
                case AuthenticationFailed:
                    message = Messages.getFormattedMessage("xnatserverauthenticationpage.authenticationfailed.message", address);
                    title = Messages.getMessage("xnatserverauthenticationpage.authenticationfailed.title");
                    break;

                case Unauthorized:
                    message = Messages.getFormattedMessage("xnatserverauthenticationpage.unauthorized.message", address);
                    title = Messages.getMessage("xnatserverauthenticationpage.unauthorized.title");
                    break;

                case NotAnXnatServer:
                    message = Messages.getFormattedMessage("xnatserverauthenticationpage.notanxnat.message", address);
                    title = Messages.getMessage("xnatserverauthenticationpage.notanxnat.title");
                    break;

                case UnverifiedHttps:
                    message = Messages.getFormattedMessage("xnatserverauthenticationpage.unverified.message", address);
                    title = Messages.getMessage("xnatserverauthenticationpage.unverified.title");
                    break;

                case EmptyAddress:
                case InvalidAddress:
                    message = Messages.getFormattedMessage("xnatserverconfigurationpanel.invalidserver.message", address);
                    title = Messages.getMessage("xnatserverconfigurationpanel.invalidserver.title");
                    break;

                case Unsupported:
                    message = Messages.getFormattedMessage("xnatserverauthenticationpage.unsupported.message", address, e.getMessage());
                    title = Messages.getMessage("xnatserverauthenticationpage.unsupported.title");
                    break;

                case SystemError:
                    message = Messages.getFormattedMessage("xnatserverauthenticationpage.systemerror.message", address, e.getMessage());
                    title = Messages.getMessage("xnatserverauthenticationpage.systemerror.title");
                    break;

                default:
                    message = Messages.getFormattedMessage("xnatserverauthenticationpage.unknown.message", address, e.getMessage());
                    title = Messages.getMessage("xnatserverauthenticationpage.unknown.title");
                    break;
            }

            JOptionPane.showMessageDialog(this, message, title, JOptionPane.WARNING_MESSAGE);
            setProblem(message);
            return WizardPanelNavResult.REMAIN_ON_PAGE;
        }
    }

    /**
     * Renders the page. This is where controls can be set up and populated, etc.
     */
    @Override
    protected void renderingPage() {
        if (_settings.getXnatServerNames().size() > 0) {
            showAuthenticationPanel();
        } else {
            showConfigurationPanel();
        }
    }

    /**
     * Validates the contents of the page controls. This checks to see if all authentication values have been provided.
     *
     * @param component The component that triggered the validation check.
     * @param event     The event that triggered the validation check.
     *
     * @return Returns <b>null</b> if the contents are valid, otherwise returns a message indicating that the user needs
     * to provide complete authentication information.
     */
    @Override
    protected String validateContents(final Component component, final Object event) {
        if (StringUtils.equals(CONFIGURATION_PANEL_NAME, _currentCard)) {
            return _configurationPanel.validateContents();
        }
        if (_settings.getXnatServerNames().size() == 0) {
            showConfigurationPanel();
            return _configurationPanel.validateContents();
        }
        return _authenticationPanel.validateContents();
    }

    public void showConfigurationPanel() {
        showConfigurationPanel(null);
    }

    public void showConfigurationPanel(final XnatServer server) {
        setCard(CONFIGURATION_PANEL_NAME);
        if (server != null) {
            _configurationPanel.setXnatServer(server);
        }
    }

    public void showAuthenticationPanel() {
        setCard(AUTHENTICATION_PANEL_NAME);
        _authenticationPanel.adjustPanelState();
    }

    /**
     * Sets the currently displayed card.
     *
     * @param card The card to display.
     */
    private void setCard(final String card) {
        _currentCard = card;
        _layout.show(this, card);
        revalidate();
    }

    private static final String AUTHENTICATION_PANEL_NAME = XnatServerAuthenticationPanel.class.getName();
    private static final String CONFIGURATION_PANEL_NAME  = XnatServerConfigurationPanel.class.getName();

    private final UploadAssistantSettings       _settings;
    private final CardLayout                    _layout;
    private final XnatServerAuthenticationPanel _authenticationPanel;
    private final XnatServerConfigurationPanel  _configurationPanel;

    private String _currentCard;

    @Override
    public void propertyChange(final PropertyChangeEvent event) {
        if (StringUtils.equals(ClearPreferencesAction.APPLICATION_SETTINGS, event.getPropertyName())) {
            refresh();
        }
    }
}
