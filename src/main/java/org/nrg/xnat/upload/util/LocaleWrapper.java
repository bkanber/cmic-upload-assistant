/*
 * upload-assistant: org.nrg.xnat.upload.util.LocaleWrapper
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.util;

import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

/**
 * It's unfortunate that this is necessary, but it is. This shows the display name of the configured locale when the
 * {@link #toString()} method is called. This allows the locale to be displayed nicely in combo boxes, while making the
 * locale available without requiring an extra lookup operation.
 */
public class LocaleWrapper {
    public LocaleWrapper(final String languageTag) {
        _locale = Locale.forLanguageTag(languageTag);
    }

    public LocaleWrapper(final Locale locale) {
        _locale = locale;
    }

    public String getValue() {
        return _locale.toLanguageTag();
    }

    public Locale toLocale() {
        return _locale;
    }

    public String toString() {
        return _locale.getDisplayName();
    }

    public static class Comparator implements java.util.Comparator<LocaleWrapper> {
        @Override
        public int compare(final LocaleWrapper first, final LocaleWrapper second) {
            // Our default is always at the top.
            if (StringUtils.equals("en-US", first.toString())) {
                return 1;
            }
            return first.toString().compareTo(second.toString());
        }
    }

    private final Locale _locale;
}
