/*
 * upload-assistant: org.nrg.xnat.upload.util.Messages
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Messages {
    public static final String VOCABULARY_ARCHIVE          = "vocabulary.archive";
    public static final String VOCABULARY_PREARCHIVE       = "vocabulary.prearchive";
    public static final String UPLOADRESULTPANEL_SUCCESS   = "uploadresultpanel.success";
    public static final String UPLOADRESULTPANEL_DEST_LINK = "uploadresultpanel.dest.link";
    public static final String ERROR_TITLE_NOURLSUPPORT    = "error.nourlsupport.title";
    public static final String ERROR_MSG_NOURLSUPPORT      = "error.nourlsupport.message";

    public static String getPageTitle(final Class<?> clazz) {
        return getResourceBundle().getString(StringUtils.lowerCase(clazz.getSimpleName()) + ".title");
    }

    public static String getPageDescription(final Class<?> clazz) {
        return getResourceBundle().getString(StringUtils.lowerCase(clazz.getSimpleName()) + ".description");
    }

    public static boolean hasMessage(final String key) {
        return getResourceBundle().containsKey(key);
    }

    /**
     * Gets a message from the current with the specified key
     *
     * @param key The key for the resource to be retrieved.
     *
     * @return The requested resource.
     */
    public static String getMessage(final String key) {
        return getResourceBundle().getString(key);
    }

    /**
     * Gets a message from the current with the specified key and formats it with the specified values.
     *
     * @param key  The key for the resource to be retrieved.
     * @param args Objects to be injected into the formatted string.
     *
     * @return The requested resource formatted with the submitted arguments.
     */
    public static String getMessage(final String key, final Object... args) {
        return MessageFormat.format(getResourceBundle().getString(key), args);
    }

    /**
     * Gets an array of messages from the current locale with the specified keys.
     *
     * @param keys The keys for the resources to be retrieved.
     *
     * @return The requested resources in the same order as specified in the parameters.
     */
    public static List<String> getMessages(final String... keys) {
        final List<String> messages = new ArrayList<>();
        for (final String key : keys) {
            messages.add(getMessage(key));
        }
        return messages;
    }

    /**
     * Gets a message formatted for HTML display. This is mostly for use in dialogs.
     *
     * @param key  The key for the resource to be retrieved.
     * @param args Objects to be injected into the formatted string.
     *
     * @return The requested resource formatted with the submitted arguments.
     */
    public static String getFormattedMessage(final String key, final Object... args) {
        return getMessage("messages.formatted", getMessage(key, args));
    }

    /**
     * Gets the currently configured locale.
     *
     * @return The currently configured locale.
     */
    public static Locale getLocale() {
        return _bundle.getLocale();
    }

    /**
     * Gets the {@link LocaleWrapper} for the currently configured locale.
     *
     * @return The currently configured locale in a {@link LocaleWrapper}.
     */
    public static LocaleWrapper getLocaleWrapper() {
        return _locales.get(getLocale().toString());
    }

    /**
     * Sets the application resource bundle to the indicated locale.
     *
     * @param locale The desired locale.
     */
    public static void setLocale(final String locale) {
        setLocale(StringUtils.isNotBlank(locale) ? Locale.forLanguageTag(locale) : Locale.forLanguageTag("en-US"));
    }

    /**
     * Sets the application resource bundle to the indicated locale.
     *
     * @param locale The desired locale.
     */
    public static void setLocale(final Locale locale) {
        if (!getSupportedLocales().containsKey(locale.toString())) {
            // TODO: Need some messaging here to indicate a bad selection, but we're restricting selection to whatever is available anyway, so not really a big deal.
            _bundle = ResourceBundle.getBundle("org.nrg.xnat.upload.Messages", Locale.forLanguageTag("en-US"));
        } else {
            _bundle = ResourceBundle.getBundle("org.nrg.xnat.upload.Messages", locale);
        }
    }

    /**
     * Gets a map of the supported locales for the application.
     *
     * @return A map containing the supported locales along with their display names.
     */
    public static Map<String, LocaleWrapper> getSupportedLocales() {
        if (_locales.size() == 0) {
            try {
                for (final Resource resource : BasicXnatResourceLocator.getResources("classpath*:org/nrg/xnat/upload/Messages*.properties")) {
                    final String filename = resource.getFilename();

                    // Ignore the default bundle: it's en-US.
                    if (!StringUtils.equals("Messages.properties", filename)) {
                        final Matcher matcher = RESOURCE_LOCALE.matcher(filename);
                        if (matcher.matches()) {
                            final String language = matcher.group("language");
                            final String dialect = matcher.group("dialect");
                            final Locale locale = Locale.forLanguageTag(language + "-" + dialect);
                            _locales.put(locale.toString(), new LocaleWrapper(locale));
                        }
                    }
                }
            } catch (IOException ignored) {
                // TODO: This shouldn't be ignored, but not really sure what to do with it. How likely is this under normal circumstances?
            }
        }
        return _locales;
    }

    public static List<LocaleWrapper> getDisplayLocales() {
        final List<LocaleWrapper> locales = Lists.newArrayList(_locales.values());
        Collections.sort(locales, LOCALE_COMPARATOR);
        return locales;
    }

    private static ResourceBundle getResourceBundle() {
        if (_bundle == null) {
            _bundle = ResourceBundle.getBundle("org.nrg.xnat.upload.Messages");
        }
        return _bundle;
    }

    private static final Pattern RESOURCE_LOCALE = Pattern.compile("^[a-zA-Z_.]+_(?<language>[a-z]{2})_(?<dialect>[A-Z]{2}).properties$");
    private static final LocaleWrapper.Comparator LOCALE_COMPARATOR = new LocaleWrapper.Comparator();
    private static final Map<String, LocaleWrapper> _locales = Maps.newHashMap();
    private static ResourceBundle _bundle;
}
