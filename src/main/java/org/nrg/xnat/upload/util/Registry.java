/*
 * upload-assistant: org.nrg.xnat.upload.util.Registry
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.util;

import java.util.Collection;

public interface Registry<T> extends Iterable<T> {
	T get(T template);
	T get(int index);
	int getIndex(T o);
	Collection<T> getAll();
	boolean isEmpty();
	int size();
}
