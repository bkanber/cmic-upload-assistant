package org.nrg.xnat.upload.util;

import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.upload.data.Subject;

public interface VisitOccurrenceI {

    List<VisitModalityI> getModalities();

    String getKey();

    String getName();

    String getCode();

    void setSubject(Subject subject);

    Subject getSubject();

    @SuppressWarnings("unused")
    class VisitComparator implements Comparator<VisitOccurrenceI> {
        @Override
        public int compare(final VisitOccurrenceI visit1, final VisitOccurrenceI visit2) {
            if (visit1.equals(visit2)) {
                return 0;
            }

            final String name1 = visit1.getName();
            final String name2 = visit2.getName();

            final boolean isName1Blank = StringUtils.isBlank(name1);
            final boolean isName2Blank = StringUtils.isBlank(name2);

            if (isName1Blank && isName2Blank) {
                return 0;
            }
            if (isName1Blank) {
                return -1;
            }
            if (isName2Blank) {
                return 1;
            }

            return name1.compareTo(name2);
        }
    }

    String getStatus();
}
