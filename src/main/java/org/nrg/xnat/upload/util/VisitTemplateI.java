package org.nrg.xnat.upload.util;

import java.util.List;

public interface VisitTemplateI {
	List<VisitOccurrenceI> getVisits();
	String getKey();
}
