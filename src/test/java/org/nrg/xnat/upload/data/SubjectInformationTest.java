/*
 * upload-assistant: org.nrg.xnat.upload.data.SubjectInformationTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.upload.data;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.nrg.xnat.upload.net.HttpURLConnectionProcessor;
import org.nrg.xnat.upload.net.RestServer;
import org.nrg.xnat.upload.net.StringResponseProcessor;
import org.nrg.xnat.upload.application.UploadAssistant;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.w3c.dom.Document;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.easymock.EasyMock.expect;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UploadAssistant.class)
@Ignore("This runs tests with windowed components which is currently not working well.")
public class SubjectInformationTest {

    // private final Logger logger =
    // LoggerFactory.getLogger(SubjectInformationTest.class);
    private static final String EXPECTED_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"
                                               + "<xnat:Subject label=\"SUBJ01_MR1\" project=\"PROJ01\" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"/>";

    private SubjectInformation info;
    private RestServer         xnat;

    @Before
    public void setUp() throws Exception {
        final Project project = new Project("PROJ01");

        xnat = mock(RestServer.class);

        mockStatic(UploadAssistant.class);
        expect(UploadAssistant.getRestServer()).andReturn(xnat);

        info = new SubjectInformation(project);
        info.setLabel("SUBJ01_MR1");
    }

    @Test
    public void shouldCreateXML() throws Exception {
        String xml = xmlToString(info.buildXML());
        assertEquals(EXPECTED_XML, xml);
    }

    @Test
    public void shouldCreateSubjectOnServerAndReturnSubject() throws Exception {

        mockHttpResponse("http://localhost:8080/xnat/data/projects/PROJ01/subjects/Demo_S00003", EXPECTED_XML).doPost(
                eq("/data/projects/PROJ01/subjects"), any(HttpURLConnectionProcessor.class));

        Subject subject = info.uploadTo();

        // subject checks
        assertEquals("SUBJ01_MR1", subject.getLabel());
        assertEquals("Demo_S00003", subject.getId());
    }

    @Test(expected = SubjectInformation.UploadSubjectException.class)
    public void shouldWrapException() throws Exception {
        doThrow(new IOException()).when(xnat).doPost(anyString(), any(HttpURLConnectionProcessor.class));

        info.uploadTo();
    }

    private String xmlToString(Document document) throws Exception {
        final StringWriter writer      = new StringWriter();
        final Transformer  transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(new DOMSource(document), new StreamResult(writer));
        return writer.toString();
    }

    /**
     * Using the mock RestServer, we inject "response" into the Response
     * Processor.
     */
    @SuppressWarnings("SameParameterValue")
    private RestServer mockHttpResponse(final String response, final String expectedRequest) throws IOException {
        return doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) throws IOException {
                // create a mock connection that provides an InputStream seeded
                // with the "response"
                HttpURLConnection connection = mock(HttpURLConnection.class);
                when(connection.getInputStream()).thenReturn(new ByteArrayInputStream(response.getBytes()));
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                when(connection.getOutputStream()).thenReturn(out);

                StringResponseProcessor processor = (StringResponseProcessor) invocation.getArguments()[1];
                processor.prepare(connection);
                processor.process(connection);

                // ensure the data sent to the server matches what we expect
                // TODO consider moving the assert out of the mocking code
                if (expectedRequest != null) {
                    assertEquals(expectedRequest, out.toString());
                }

                return null;
            }
        }).when(xnat);
    }
}
