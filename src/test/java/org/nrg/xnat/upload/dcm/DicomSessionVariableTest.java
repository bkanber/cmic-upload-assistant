/*
 * upload-assistant: org.nrg.xnat.upload.dcm.DicomSessionVariableTest
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.upload.dcm;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.dcm4che2.data.DicomObject;
import org.junit.Ignore;
import org.junit.Test;
import org.nrg.dicom.mizer.exceptions.MultipleInitializationException;
import org.nrg.dicom.mizer.exceptions.ScriptEvaluationException;
import org.nrg.dicom.mizer.objects.DicomObjectI;
import org.nrg.dicom.mizer.values.ConstantValue;
import org.nrg.dicom.mizer.values.Value;
import org.nrg.dicom.mizer.variables.BasicVariable;
import org.nrg.dicom.mizer.variables.Variable;
import org.nrg.xnat.upload.data.SessionVariable;
import org.nrg.xnat.upload.data.SessionVariable.InvalidValueException;
import org.nrg.xnat.upload.data.ValueListener;
import org.nrg.xnat.upload.data.ValueValidator;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class DicomSessionVariableTest {
    private DicomSessionVariable makeDicomSessionVariable(final Variable variable, final DicomObjectI dicomObject) {
        return new DicomSessionVariable(variable, dicomObject) {
            public void refresh() {
            }

            public Component getEditor() {
                return null;
            }
        };
    }

    /**
     * Test method for {@link DicomSessionVariable#getSessionVariable(Variable, DicomObject)}.
     */
    @Test
    public void testGetSessionVariable() {
        final String   name     = "foo";
        final Variable variable = mock(Variable.class);
        when(variable.getName()).thenReturn(name);
        final DicomSessionVariable dicomSessionVariable = DicomSessionVariable.getSessionVariable(variable, mock(DicomObject.class));
        assertEquals(name, dicomSessionVariable.getName());
    }

    /**
     * Test method for {@link DicomSessionVariable#getValue()}.
     */
    @Test
    public void testGetValue() throws InvalidValueException {
        final ConstantValue value    = new ConstantValue("foo");
        final Variable      variable = mock(Variable.class);
        when(variable.getValue()).thenReturn(value);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertEquals(value, dicomSessionVariable.getValue());
    }

    /**
     * Test method for {@link DicomSessionVariable#getValue()}.
     */
    @Test
    public void testGetNoValue() throws InvalidValueException {
        final Variable variable = mock(Variable.class);
        when(variable.getValue()).thenReturn(null);
        when(variable.getInitialValue()).thenReturn(null);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertNull(dicomSessionVariable.getValue());
    }

    /**
     * Test method for {@link DicomSessionVariable#getValue()}.
     */
    @Test
    public void testGetValueFromInitialValue() throws InvalidValueException, ScriptEvaluationException {
        final String        textValue    = "foo";
        final ConstantValue value        = new ConstantValue(textValue);
        final Variable      variable     = mock(Variable.class);
        final Value         initialValue = mock(Value.class);
        final DicomObjectI  dicomObject  = mock(DicomObjectI.class);
        when(variable.getValue()).thenReturn(null);
        when(variable.getInitialValue()).thenReturn(initialValue);
        when(initialValue.on(dicomObject)).thenReturn(textValue);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, dicomObject);
        assertEquals(value, dicomSessionVariable.getValue());
    }

    /**
     * Test method for {@link DicomSessionVariable#editTo(java.lang.String)}.
     */
    @Test
    public void testEditTo() {
        final String        newValue = "yak";
        final ValueListener listener = mock(ValueListener.class);
        final Variable      variable = mock(Variable.class);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        dicomSessionVariable.addListener(listener);
        dicomSessionVariable.editTo(newValue);
        verify(variable).setValue(newValue);
        verify(listener).hasChanged(dicomSessionVariable);
    }

    /**
     * Test method for {@link DicomSessionVariable#editTo(java.lang.String)}.
     */
    @Test
    @Ignore("Not sure if this ever actually worked: appears to be a problem with an incomplete mock, so skipping.")
    public void testEditToInvalid() {
        final String         bad       = "bad";
        final ValueListener  listener  = mock(ValueListener.class);
        final ValueValidator validator = mock(ValueValidator.class);
        final Variable       variable  = mock(Variable.class);
        when(validator.isValid(bad)).thenReturn(false);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        dicomSessionVariable.addValidator(validator);
        dicomSessionVariable.addListener(listener);
        dicomSessionVariable.editTo(bad);
        verify(listener).isInvalid(eq(dicomSessionVariable), eq(variable), anyString());
        verify(variable, never()).setValue(anyString());
    }

    /**
     * Test method for {@link DicomSessionVariable#isHidden()}.
     */
    @Test
    public void testIsHiddenFalse() {
        final Variable variable = mock(Variable.class);
        when(variable.isHidden()).thenReturn(false);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertFalse(dicomSessionVariable.isHidden());
        verify(variable).isHidden();
    }

    /**
     * Test method for {@link DicomSessionVariable#isHidden()}.
     */
    @Test
    public void testIsHiddenTrue() {
        final Variable variable = mock(Variable.class);
        when(variable.isHidden()).thenReturn(true);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertTrue(dicomSessionVariable.isHidden());
        verify(variable).isHidden();
    }

    /**
     * Test method for {@link DicomSessionVariable#setIsHidden(boolean)}.
     */
    @Test
    public void testSetIsHidden() {
        final Variable variable = mock(Variable.class);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        dicomSessionVariable.setIsHidden(true);
        verify(variable).setIsHidden(true);
    }

    /**
     * Test method for {@link DicomSessionVariable#setValue(java.lang.String)}.
     */
    @Test
    public void testSetValue() throws InvalidValueException {
        final String   oldValue = "foo";
        final String   newValue = "bar";
        final Variable variable = mock(Variable.class);
        when(variable.setValue(newValue)).thenReturn(oldValue);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertEquals(oldValue, dicomSessionVariable.setValue(newValue));
        verify(variable).setValue(newValue);
    }

    /**
     * Test method for {@link DicomSessionVariable#setInitialValue(Value)}.
     */
    @Test
    public void testSetInitialValue() throws MultipleInitializationException {
        final Value    value    = mock(Value.class);
        final Variable variable = mock(Variable.class);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        dicomSessionVariable.setInitialValue(value);
        verify(variable).setInitialValue(value);
    }

    /**
     * Test method for {@link DicomSessionVariable#setInitialValue(Value)}.
     */
    @Test(expected = MultipleInitializationException.class)
    public void testSetInitialValueMultiple() throws MultipleInitializationException {
        final Value    value    = mock(Value.class);
        final Variable variable = mock(Variable.class);
        doThrow(new MultipleInitializationException(variable, value, value)).when(variable).setInitialValue(value);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        dicomSessionVariable.setInitialValue(value);
    }


    /**
     * Test method for {@link DicomSessionVariable#getTags()}.
     */
    @Test
    public void testGetTagsNoInitialValue() {
        final Variable variable = mock(Variable.class);
        when(variable.getInitialValue()).thenReturn(null);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertTrue(dicomSessionVariable.getTags().isEmpty());
    }

    /**
     * Test method for {@link DicomSessionVariable#getTags()}.
     */
    @Test
    public void testGetTagsWithInitialValue() {
        final SortedSet<Long> tags     = new TreeSet<>(ImmutableSet.of(1L, 2L, 3L, 4L));
        final Variable        variable = mock(Variable.class);
        final Value           value    = mock(Value.class);
        when(variable.getInitialValue()).thenReturn(value);
        when(value.getTags()).thenReturn(tags);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertEquals(tags, dicomSessionVariable.getTags());
    }

    /**
     * Test method for {@link DicomSessionVariable#getVariables()}.
     */
    @Test
    public void testGetVariablesNoInitialValue() {
        final Variable variable = mock(Variable.class);
        when(variable.getInitialValue()).thenReturn(null);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertTrue(dicomSessionVariable.getVariables().isEmpty());
    }

    /**
     * Test method for {@link DicomSessionVariable#getVariables()}.
     */
    @Test
    public void testGetVariablesWithInitialValue() {
        final Set<Variable> variables = getVariableSet("some", "arbitrary", "objects", "4");
        final Variable      variable  = mock(Variable.class);
        final Value         value     = mock(Value.class);
        when(variable.getInitialValue()).thenReturn(value);
        when(value.getVariables()).thenReturn(variables);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        assertEquals(variables, dicomSessionVariable.getVariables());
    }

    /**
     * Test method for {@link DicomSessionVariable#hasChanged(SessionVariable)}.
     */
    @Test
    public void testHasChanged() {
        final ValueListener listener = mock(ValueListener.class);
        final Variable      variable = mock(Variable.class);

        final DicomSessionVariable dicomSessionVariable = makeDicomSessionVariable(variable, mock(DicomObjectI.class));
        dicomSessionVariable.addListener(listener);
        dicomSessionVariable.hasChanged(dicomSessionVariable);
        verify(listener).hasChanged(dicomSessionVariable);
    }

    private Set<Variable> getVariableSet(final String... names) {
        final HashSet<Variable> variables = Sets.newHashSet();
        for (final String name : names) {
            variables.add(new BasicVariable(name));
        }

        return ImmutableSet.copyOf(variables);
    }
}
